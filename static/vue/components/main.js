Vue.component('poliza-validor', {
  data(){
      return {
        anonymous:null,
        results:[],
        num:null,
        rut:null,
        found:true,
        make:["Chevrolet","Hyundai","Toyota","Peugeot","Nissan","Kia","Mazda","Koenigsegg","Mitsubishi","Chrysler","Ford","Chevrolet","Dodge"],
      }
    },
  props: ['endpoint','nexturl','button_text','anonymous_mode'],
  template: `
  <div class="tab-content">
    <div class="poliza bg-white" method="post" action="">
      <h6 v-if="anonymous" class="consulta_poliza">Consulta <strong>Siniestro</strong></h6>
      <h6 v-else class="consulta_poliza">Consulta <strong>Póliza</strong></h6>
      <br>
      <br>
      <div class="form-group">
        <label for="rut">RUT</label>
        <input v-model="rut" type="text" id="rut" aria-describedby="rut" maxlength="50" name="rut" placeholder="RUT Asegurado" class="form-control error" aria-required="true" aria-invalid="true">
      </div> 
        <br>
        <div style="background-color: red;color: white;">
      </div>
      <div result>
        <div v-if="anonymous">
          <h6 v-if="num==null && rut == null" class="consulta_poliza"><strong>Nota:</strong>Si tu RUT no es valido no podras consultar los siniestros</h6>
        </div>
        <div v-else>
          <h6 v-if="num==null && rut == null" class="consulta_poliza"><strong>Nota:</strong>Si tu póliza no es válida no puedes continuar con el proceso.</h6>
        </div>
        <h6 v-if="!found | ( (num | rut ) & isEmpty)" class="consulta_poliza" style="color:red;"><strong>No tenemos registros</strong></h6>
        <div v-if="!isEmpty">
        <div class="client" style="margin-bottom:3em;border:1px solid;text-align:center;">
        <p><b>Datos del Cliente:</b></p>
        <p>Nombre: {{results[0].cliente.nombre}}</p>
        <p>Apellidos: {{results[0].cliente.apellido}}</p>
        <p>Fecha Nacimiento: {{results[0].cliente.fecha_nacimiento}}</p>
        <p>RUT: {{results[0].cliente.rut}}</p>
        <p>Numero Documento: {{results[0].cliente.nro_doc}}</p>
        <p>Direccion: {{results[0].cliente.direccion}}</p>
        <p>Email: {{results[0].cliente.email}}</p>
        <p>Telefono: {{results[0].cliente.telefono}}</p>
        </div>
          <div v-for="row in results" style="border: 1px solid;margin-bottom:1em;text-align:center;">
            <p><b>Datos de la poliza:</b></p>
            <p>Numero Poliza: {{row.numero_poliza}}</p>
            <p>Monto Cobertura: {{ formatAmount(row.cantidad_maxima) }}</p>
            <div v-if="anonymous && row.siniestro">
              <p><b>Datos del Siniestro:</b></p>
              <p># Siniestro: {{row.siniestro.id}}</p>
              <p>Fecha: {{row.siniestro.fecha}}</p>
              <p>Estado: {{row.siniestro.estado}}</p>
            </div>
            <div v-else-if="!anonymous">
              <p><b>Datos del Vehiculo:</b></p>
              <p>Marca: {{make[row.vehiculo.marca]}}</p>
              <p>Modelo: {{row.vehiculo.modelo}}</p>
              <p>Nuevo: {{row.vehiculo.nuevo}}</p>
              <p>Patente: {{row.vehiculo.patente}}</p>
              <p>Año: {{row.vehiculo.year}}</p>
              <a :href="getNexUrl(row.id)" class="btn btn-primary">{{button_text}}</a>
            </div>
            <div v-else>
              <p style="color:red;"><b>No existen Registros</b></p>
            </div>
          </div>
        </div>
      </div>
    </div>
      <br>
      <br>
      <br>
      <br>
      <br>
    </div>
  `,
  mounted() {
    this.anonymous = (this.anonymous_mode === 'true')
  },
  watch: {
    num: function (val) {
      this.getData();
    },
    rut: function (val) {
      this.getData();
    },
  },
  methods: {
      getData : function (){
          let self = this;
          self.results = [];
          return axios.get(self.endpoint+"?&rut="+self.rut)
            .then(function (response) {
              self.found = true;
              self.results = response.data;
            }).catch(function (error) {
              self.found = false;
            });
      },
      formatAmount : function (amount){
        let self = this;
        let options = { style: 'currency', currency: 'CLP' };
        let numberFormat = new Intl.NumberFormat('es-CL', options);

        return numberFormat.format(amount);
      },
      getNexUrl:function(id){
        // remove fake record 0
        let tmp_nexturl = this.nexturl.slice(0,-1);
  
        if(Object.keys(this.results).length === 0){
          return tmp_nexturl;
        }else{
          return tmp_nexturl+id;
        }
      }
  },
  computed: {
    isEmpty: function () {
      return Object.keys(this.results).length === 0;
    },
  }
});

Vue.component('cotizador', {
  data(){
      return {
        errors: [],
        form:{
          "montoCobertura":0,
          "montoTotal":0,
          "siniestro":0,
          "observaciones":null,
          "tiempoRepDias":1,
          "details":[{
            "nombre":null,
            "descripcion":null,
            "monto":0,
          }],
      },
    }
  },
  props: ['max_amount','pk_siniestro','token','store_url','success_url'],
  template: `
      <div class="container-fluid">
      <div style="color: red;text-align: center;">
      <p v-if="errors.length">
      <b>Por favor revise los siguientes error(es):</b>
      <ul>
        <li v-for="error in errors">{{ error }}</li>
      </ul>
    </p>
    </div>
      <div class="form-row" data-select2-id="4">
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_montoCobertura">Monto Cobertura</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.montoCobertura"
              v-bind:readonly="true"
              name="montoCobertura"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_montoCobertura"
            />
            <small class="text-muted"></small>
          </div>
        </div>
      
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_montoTotal">Monto Total</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.montoTotal"
              v-bind:readonly="true"
              name="montoTotal"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_montoTotal"
            />
            <small class="text-muted"></small>
          </div>
        </div>
      
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_tiempoRepDias">Tiempo en dias de reparacion</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.tiempoRepDias"
              name="tiempoRepDias"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_tiempoRepDias"
            />
            <small class="text-muted"></small>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
        <label for="id_observaciones">Observaciones</label><br />
    
        <div class="form-group">
          <textarea
            type="number"
            v-model="form.observaciones"
            name="observaciones"
            class="form-control"
            autocomplete="off"
            required=""
            id="id_observaciones"
          />
          <small class="text-muted"></small>
        </div>
      </div>

  
      </div>
      <div class="row" data-select2-id="4">
        <div class="col-xs-3 col-sm-3 col-md-3">
          <button class="btn btn-success" @click.prevent="addDetail()">Agregar Detalle</button>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
          <button v-if="removible" class="btn btn-danger" @click.prevent="removeDetail()">Remover Detalle</button>
        </div>
      </div>
      <div class="form-row" data-select2-id="4" v-for="(row, index) in form.details">

        <div class="col-xs-3 col-sm-3 col-md-3">
          <label for="id_detalles">nombre</label><br />
      
          <div class="form-group">
            <input type="text" name="nombre" v-model="row.nombre"></input>
            <small class="text-muted"></small>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6">
        <label for="id_detalles">descripcion</label><br />
    
        <div class="form-group">
          <input type="text" name="descripcion" style="width: 100%;" v-model="row.descripcion"></input>
          <small class="text-muted"></small>
        </div>
      </div>

      <div class="col-xs-3 col-sm-3 col-md-3">
        <label for="id_detalles">monto aproximado</label><br />
  
        <div class="form-group">
          <input type="number" name="monto" v-model="row.monto"></input>
          <small class="text-muted"></small>
        </div>
      </div>

      </div>
      
      <button class="btn btn-success" @click.prevent="sendForm()">Guardar</button>
    </div>
  `,
  mounted() {
    this.form.montoCobertura = this.cleanAmount(this.max_amount);
    this.form.siniestro = this.pk_siniestro;
  },
  watch: {
    form:{
        // This will let Vue know to look inside the array
        deep: true,

        // We have to move our method to a handler field
        handler(value){
          this.updateAmount();
        }
    },
  },
  methods: {
    cleanAmount:function(amount){
      let tmp_amount = amount.split(",");
      return tmp_amount[0];
    },
    execedAmount:function(){
      return this.form.montoCobertura < this.form.montoTotal;
    },
      validForm:function(){
        this.errors = [];
        for ( let key in this.form ) {
          if(typeof(this.form[key])=="object"){
            for ( let tmp_key in this.form[key] ) {
              for ( let key_2 in this.form[key][tmp_key] ) {
                if(!this.form[key][tmp_key][key_2]){
                  this.errors.push(key_2+' en la fila '+(parseInt(tmp_key)+1)+' no puede estar vacio.');
                }
              }
            }
            }else{
              if(!this.form[key]){
                this.errors.push(key+' no puede estar vacio.');
              }
            }
        }
        if (!this.errors.length) {
          return true;
        }
        return false;
      },
      addDetail:function(){
          this.form.details.push({
           "nombre":null,
           "descripcion":null,
           "monto":0,
          });
      },
      removeDetail:function(){
        this.form.details.pop();
      },
      updateAmount(){
        let tmp_amount = 0;
        for(let x=0;x<this.form.details.length;x++){
          let row = this.form.details[x];
          tmp_amount = tmp_amount + parseInt(row.monto);
        }
        this.form.montoTotal = tmp_amount;
      },
      sendForm(){
        if(!this.validForm()){
          Swal.fire({
            icon: 'error',
            title: 'Formulario Incompleto',
            text: 'Por favor llena todos los campos de la formas',
          });
          return false;
        }
        if(this.execedAmount()){
          Swal.fire({
            icon: 'error',
            title: 'Monto Total',
            text: 'El monto total supera al maximo de la poliza',
          });
          return false;
        }
        let self = this;
        let form_data = new FormData();
        for ( let key in this.form ) {
          if(typeof(this.form[key])=="object"){
            for ( let tmp_key in this.form[key] ) {
              for ( let key_2 in this.form[key][tmp_key] ) {
                form_data.append(key_2, this.form[key][tmp_key][key_2]);
              }
            }
            }else{
              form_data.append(key, this.form[key]);
            }
        }
        let config = {headers:{'X-CSRFToken':this.token}};
        axios.post(this.store_url,form_data,config).then(function (response) {
          Swal.fire({
            icon: 'success',
            title: 'Registrado!!!',
            text: 'Cotizacion Creada con Exito',
            onClose: function () {
              window.location.href = self.success_url;
            },
          });
        }).catch(function (error) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'tuvimos algunos problemas con lo siguiente'+error.response.data.details,
          });
        });
      },
  },
  computed: {
    removible: function () {
      return Object.keys(this.form.details).length >= 2;
    },
  }
});


Vue.component('editor-cotizador', {
  data(){
      return {
        next_url:null,
        errors: [],
        form:{
          "id":null,
          "montoCobertura":0,
          "montoTotal":0,
          "siniestro":0,
          "observaciones":null,
          "tiempoRepDias":1,
          "details":[{
            "id":null,
            "nombre":null,
            "descripcion":null,
            "monto":0,
          }],
      },
    }
  },
  props: ['max_amount','pk_siniestro','token','store_url','success_url','detail_url','pk_fix'],
  template: `
      <div class="container-fluid">
      <div style="color: red;text-align: center;">
      <p v-if="errors.length">
      <b>Por favor revise los siguientes error(es):</b>
      <ul>
        <li v-for="error in errors">{{ error }}</li>
      </ul>
    </p>
    </div>
      <div class="form-row" data-select2-id="4">
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_montoCobertura">Monto Cobertura</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.montoCobertura"
              v-bind:readonly="true"
              name="montoCobertura"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_montoCobertura"
            />
            <small class="text-muted"></small>
          </div>
        </div>
      
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_montoTotal">Monto Total</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.montoTotal"
              v-bind:readonly="true"
              name="montoTotal"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_montoTotal"
            />
            <small class="text-muted"></small>
          </div>
        </div>
      
        <div class="col-xs-4 col-sm-4 col-md-4">
          <label for="id_tiempoRepDias">Tiempo en dias de reparacion</label><br />
      
          <div class="form-group">
            <input
              type="number"
              v-model="form.tiempoRepDias"
              name="tiempoRepDias"
              value="0"
              min="0"
              class="form-control"
              autocomplete="off"
              required=""
              id="id_tiempoRepDias"
            />
            <small class="text-muted"></small>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
        <label for="id_observaciones">Observaciones</label><br />
    
        <div class="form-group">
          <textarea
            type="number"
            v-model="form.observaciones"
            name="observaciones"
            class="form-control"
            autocomplete="off"
            required=""
            id="id_observaciones"
          />
          <small class="text-muted"></small>
        </div>
      </div>

  
      </div>
      <div class="row" data-select2-id="4">
        <div class="col-xs-3 col-sm-3 col-md-3">
          <button class="btn btn-success" @click.prevent="addDetail()">Agregar Detalle</button>
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3">
          <button v-if="removible" class="btn btn-danger" @click.prevent="removeDetail()">Remover Detalle</button>
        </div>
      </div>
      <div class="form-row" data-select2-id="4" v-for="(row, index) in form.details">

        <div class="col-xs-3 col-sm-3 col-md-3">
          <label for="id_detalles">nombre</label><br />
      
          <div class="form-group">
            <input type="text" name="nombre" v-model="row.nombre"></input>
            <small class="text-muted"></small>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6">
        <label for="id_detalles">descripcion</label><br />
    
        <div class="form-group">
          <input type="text" name="descripcion" style="width: 100%;" v-model="row.descripcion"></input>
          <small class="text-muted"></small>
        </div>
      </div>

      <div class="col-xs-3 col-sm-3 col-md-3">
        <label for="id_detalles">monto aproximado</label><br />
  
        <div class="form-group">
          <input type="number" name="monto" v-model="row.monto"></input>
          <small class="text-muted"></small>
        </div>
      </div>

      </div>
      
      <button class="btn btn-success" @click.prevent="sendForm()">Guardar</button>
    </div>
  `,
  mounted() {
    let self = this;
    this.form.montoCobertura = this.cleanAmount(this.max_amount);
    this.form.siniestro = this.pk_siniestro;
    axios.get(this.detail_url+this.pk_fix).then(function (response) {
      self.form = response.data;
      self.next_url = self.success_url.slice(0,-1)+self.form.id;
    });
  },
  watch: {
    form:{
        // This will let Vue know to look inside the array
        deep: true,

        // We have to move our method to a handler field
        handler(value){
          this.updateAmount();
        }
    },
  },
  methods: {
    cleanAmount:function(amount){
      let tmp_amount = amount.split(",");
      return tmp_amount[0];
    },
    execedAmount:function(){
      return this.form.montoCobertura < this.form.montoTotal;
    },
      validForm:function(){
        this.errors = [];
        for ( let key in this.form ) {
          if(typeof(this.form[key])=="object"){
            for ( let tmp_key in this.form[key] ) {
              for ( let key_2 in this.form[key][tmp_key] ) {
                if(!this.form[key][tmp_key][key_2]){
                  if(key_2 != 'id'){
                    this.errors.push(key_2+' en la fila '+(parseInt(tmp_key)+1)+' no puede estar vacio.');
                  }
                }
              }
            }
            }else{
              if(!this.form[key]){
                this.errors.push(key+' no puede estar vacio.');
              }
            }
        }
        if (!this.errors.length) {
          return true;
        }
        return false;
      },
      addDetail:function(){
          this.form.details.push({
            "id":null,
           "nombre":null,
           "descripcion":null,
           "monto":0,
          });
      },
      removeDetail:function(){
        this.form.details.pop();
      },
      updateAmount(){
        let tmp_amount = 0;
        if(this.form.hasOwnProperty('details')){
          for(let x=0;x<this.form.details.length;x++){
            let row = this.form.details[x];
            tmp_amount = tmp_amount + parseInt(row.monto);
          } 
        }
        this.form.montoTotal = tmp_amount;
      },
      sendForm(){
        if(!this.validForm()){
          Swal.fire({
            icon: 'error',
            title: 'Formulario Incompleto',
            text: 'Por favor llena todos los campos de la formas',
          });
          return false;
        }
        if(this.execedAmount()){
          Swal.fire({
            icon: 'error',
            title: 'Monto Total',
            text: 'El monto total supera al maximo de la poliza',
          });
          return false;
        }
        let self = this;
        let form_data = new FormData();
        for ( let key in this.form ) {
          if(typeof(this.form[key])=="object"){
            for ( let tmp_key in this.form[key] ) {
              for ( let key_2 in this.form[key][tmp_key] ) {
                form_data.append(key_2, this.form[key][tmp_key][key_2]);
              }
            }
            }else{
              form_data.append(key, this.form[key]);
            }
        }
        let config = {headers:{'X-CSRFToken':this.token}};
        axios.post(this.store_url,self.form,config).then(function (response) {
          Swal.fire({
            icon: 'success',
            title: 'Actualizado',
            text: 'Cotizacion modificada con exito',
            onClose: function () {
              window.location.href = self.next_url;
            },
          });
        }).catch(function (error) {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'tuvimos algunos problemas con lo siguiente '+error.response.data.details,
          });
        });
      },
  },
  computed: {
    removible: function () {
        return Object.keys(this.form.details).length >= 2;
      return false;
    },
  }
});



new Vue({
    el: "#vue-app",
});
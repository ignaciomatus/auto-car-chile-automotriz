function MensajeError(obj) {
    var html = '';
    if (typeof(obj) === 'object') {
        html = '<ul style="text-align: left;">';
        $.each(obj, function(key, value) {
            html += '<li>' + key + ': ' + value + '</li>';
        });
        html += '</ul>';
    } else {
        html = '<p>' + obj + '</p>';
    }
    Swal.fire({
        title: 'Error!',
        html: html,
        icon: 'error'
    });
}


function ConsultaAjax(parametros, funcionExito, funcionError) {
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        data: parametros,
        dataType: 'json',
        headers: {
            "X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()
        },
        dataType: 'json',
    }).done(function(data) {
        if (!data.hasOwnProperty('error')) {
            funcionExito(data);
            return false;
        }
        funcionError(data);
        MensajeError(data.error);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        alert(textStatus + ': ' + errorThrown);
    }).always(function(data) {});
}

function formAJAX(url, titulo, textoContenido, parametros, funcionExito) {
    $.confirm({
        theme: 'material',
        title: titulo,
        icon: 'fa fa-info',
        content: textoContenido,
        columnClass: 'small',
        typeAnimated: true,
        cancelButtonClass: 'btn-primary',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            info: {
                text: "Si",
                btnClass: 'btn-primary',
                action: function() {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: parametros,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        headers: {
                            "X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()
                        },
                        dataType: 'json',
                    }).done(function(data) {
                        if (!data.hasOwnProperty('error')) {
                            funcionExito(data);
                            return false;
                        }
                        MensajeError(data.error);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        alert(textStatus + ': ' + errorThrown);
                    }).always(function(data) {});
                }
            },
            danger: {
                text: "No",
                btnClass: 'btn-red',
                action: function() {

                }
            },
        }
    })
}

$('.select2Multiple').select2({
    closeOnSelect: false,
    theme: 'bootstrap4',
    width: "100%",
});

$('.select2').select2({
    closeOnSelect: true,
    theme: 'bootstrap4',
    width: "100%",
});

function alertJS(icono, url, titulo, contenido, parametros, callback) {
    //onsole.log('url ', url);
    $.confirm({
        theme: 'bootstrap',
        title: titulo,
        icon: icono,
        content: contenido,
        columnClass: 'small',
        typeAnimated: true,
        cancelButtonClass: 'btn-primary',
        draggable: true,
        dragWindowBorder: false,
        buttons: {
            info: {
                text: "Si",
                btnClass: 'btn-primary',
                action: function() {
                    //ConsultaAjax(parametros, callback(data),)

                    $.ajax({
                        url: url, //window.location.pathname,
                        type: 'POST',
                        data: parametros,
                        headers: {
                            "X-CSRFToken": $("[name=csrfmiddlewaretoken]").val()
                        },
                        dataType: 'json'
                    }).done(function(data) {
                        //console.log("Hola");
                        if (!data.hasOwnProperty('error')) {
                            callback();
                            //location.href = '{{ url }}';
                            return false;
                        }
                        MensajeError(data.error);
                    }).fail(function(jqXHR, textStatus, errorThrown) {

                    }).always(function(data) {});
                }
            },
            danger: {
                text: "No",
                btnClass: 'btn-red',
                action: function() {

                }
            },
        }
    });
}


$(".tablaActual").on('click', '.eliminar', function() {
    var url = $(this).attr('data-url');
    var parametros = $(this).attr('datos');
    alertJS(
        'fas fa-trash-alt',
        url,
        'Eliminar registro',
        '¿Estas seguro que deseas eliminar el registro?',
        parametros,
        function(data) {
            location.reload();
        }
    );
});
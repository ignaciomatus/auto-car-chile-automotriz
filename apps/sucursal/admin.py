from django.contrib import admin

from .models import GruaModel, TallerModel
admin.site.register(TallerModel) 

admin.site.register(GruaModel)
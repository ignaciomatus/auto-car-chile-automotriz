from django.urls import path, include
from .views.viewsComuna import ListComunaView, AddComunaView, ChangeComunaView, DeleteComunaView
from .views.views import ListSucursalView, AddSucursalView, ChangeSucursalView, DeleteSucursalView

from .views.viewsTaller import ListTallerView,AddTallerView,ChangeTallerView,DeleteTallerView, ListCarTallerView, buscar_taller
from .views.viewsGrua import ListGruaView,AddGruaView,ChangeGruaView,DeleteGruaView,AceptarGruaView, buscar_grua
from django.conf.urls import url

urlpatterns = [
    path('sucursal/list', ListSucursalView.as_view(), name="listSucursal"),
    path('sucursal/register', AddSucursalView.as_view(), name="addSucursal"),
    path('sucursal/change/<int:pk>/',ChangeSucursalView.as_view(),name = 'changeSucursal'),
    path('sucursal/delete/<int:pk>/',DeleteSucursalView.as_view(),name = 'deleteSucursal'),

    path('comuna/list', ListComunaView.as_view(), name="listComuna"),
    path('comuna/register', AddComunaView.as_view(), name="addComuna"),
    path('comuna/change/<int:pk>/',ChangeComunaView.as_view(),name = 'changeComuna'),
    path('comuna/delete/<int:pk>/',DeleteComunaView.as_view(),name = 'deleteComuna'),

    path('taller/list', ListTallerView.as_view(), name="listTaller"),
    path('taller/register', AddTallerView.as_view(), name="addTaller"),
    path('taller/change/<int:pk>/',ChangeTallerView.as_view(),name = 'changeTaller'),
    path('taller/delete/<int:pk>/',DeleteTallerView.as_view(),name = 'deleteTaller'),

    path('taller/list/car', ListCarTallerView.as_view(), name="listTallerCar"),

    path('taller/buscar',buscar_taller,name = 'buscarTaller'),

    path('grua/list', ListGruaView.as_view(), name="listGrua"),
    path('grua/register', AddGruaView.as_view(), name="addGrua"),
    path('grua/change/<int:pk>/',ChangeGruaView.as_view(),name = 'changeGrua'),
    path('grua/delete/<int:pk>/',DeleteGruaView.as_view(),name = 'deleteGrua'),

    path('grua/buscar',buscar_grua,name = 'buscarGrua'),
    path('grua/confirmar/<int:pk>/',AceptarGruaView.as_view(),name = 'confirmarGrua'),
]

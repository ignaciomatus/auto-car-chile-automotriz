$(function(){
    $(".tablaActual").on('click', '.cambio', function() {
        var url = $(this).attr('data-url');
        var action =  $(this).attr('action');
        var titulo = 'Aceptar automovil';  
        var texto = '¿El automovil ha llegado correctamente? En caso de que exista algún inconveniente, favor de contactar a la sucursal antes de continuar con el proceso';
        var logo = 'fas fa-check';
        
        var parametros = {
            'pk' : $(this).attr('datos'),
            'action' : action,
        }
        alertJS(
           logo,
            url,
            titulo,
            texto,
            parametros,
            function(data) {
                location.reload();
            }
        );
    });
    $(".tablaActual").on('click', '.finish', function() {
        var url = $(this).attr('data-url');
        var action =  $(this).attr('action');
        var titulo = 'Entregar Vehículo';  
        var texto = 'Si el vehículo se encuentra reparado debe continuar con el proceso, en caso contrario Favor de cancelar.';
        var logo = 'fas fa-check';
        
        var parametros = {
            'pk' : $(this).attr('datos'),
            'action' : action,
        }
        alertJS(
           logo,
            url,
            titulo,
            texto,
            parametros,
            function(data) {
                location.reload();
            }
        );
    });
})
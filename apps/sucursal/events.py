from django.contrib.sites.shortcuts import get_current_site
from django.utils import dateformat, formats
from apps.accidents.models import Accident
from apps.core.mixins import EnviarCorreo


def correo_asignacion_chofer(accidente:Accident,request=None):
    correo_object = EnviarCorreo()
    parametrosHTML = {'fecha': dateformat.format(accidente.fecha, formats.get_format('l/d/F/Y')),
        'direccion': accidente.direccion_incidente,
        'detalles': accidente.detalles,
        'tallerNombre': accidente.taller.nombre,
        'tallerDireccion': accidente.taller.direccion,
        'chofer' : accidente.grua.chofer.get_full_name(),
        'numeroPoliza' : accidente.poliza.numero_poliza,
        "dominio" : get_current_site(request).domain if request else '127.0.0.1:8000',
        'grua' : accidente.grua,
        'accidente': accidente,
    }
    titulo = 'Tenemos un nuevo servicio para tí'
    template = 'grua/enviarGrua.html'
    correos = ["l.ignaciomatus@gmail.com","williagut@gmail.com","jos.aravenas@alumnos.duoc.cl",accidente.grua.chofer.email]
    correo_object.enviarCorreo(correos,'Ahoy tienes un trabajo!!!',template,parametrosHTML)
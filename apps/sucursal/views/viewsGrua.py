from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo, EnviarCorreo
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from django.views.decorators.http import require_http_methods
from django.shortcuts import render, HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.http.response import JsonResponse
from django.utils import dateformat, formats
from apps.accidents.models import Accident
from django.urls import reverse_lazy
from ..models import GruaModel
from ..forms import GruaForm

class AceptarGruaView(TemplateView):
    template_name = 'grua/confirmarGrua.html'
    def get(self,*args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['tituloPagina'] = "Grua en camino"
        
        try:
            print("entra aca")
            datos = self.request.GET.get('p', -1).split("?")
            numeroDoc = datos[0]
            poliza = datos[1].split("=")[1]
            accidente = Accident.objects.filter(poliza__numero_poliza=poliza,
                pk=self.kwargs['pk'])
            if accidente.exists(): 
                accidente = accidente.first()
                context['accidente'] = accidente
                print(accidente.estado)
                if accidente.estado > 3:
                    context['noDisponible'] = True
                accidente.estado = 2
                accidente.save()

                return self.render_to_response(context)
        except Exception as e:
            pass
        context['error'] = 'El link es incorrecto. Favor de verificarlo a la sucursal de origen'
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Grua en camino'
        context['tituloSeccion'] = context['tituloPagina']
        context['titulo'] = 'gruas'
        return context

class ListGruaView(PermisosRequeridos,EnviarCorreo, ListView):
    model = GruaModel
    template_name='appSucursal/listGrua.html'
    permission_required = ['sucursal.view_grua']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de gruas registradas'
        context['tituloSeccion'] = context['tituloPagina']
        context['titulo'] = 'gruas'
        context['url'] = reverse_lazy('listGrua')
        return context

class AddGruaView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = GruaModel
    form_class = GruaForm
    permission_required =   ['sucursal.add_grua']
    success_url = reverse_lazy('listGrua')
    mensajeExito = 'Grua registrada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar grua'
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['columnaForm'] = True
        context['url'] = reverse_lazy('listGrua')
        return context

class ChangeGruaView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'base/baseForm.html'
    model = GruaModel
    form_class = GruaForm
    permission_required =   ['sucursal.change_grua']
    success_url = reverse_lazy('listGrua')
    mensajeExito = 'Datos de grua actualizados exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar grua con patente ' + str(self.object.patente)
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context['botonEnviar'] =  "Actualizar grua"
        context["action"] = "edit"
        context['columnaForm'] = True
        context['url'] = reverse_lazy('listGrua')
        return context

class DeleteGruaView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = GruaModel
    permission_required =   ['sucursal.delete_grua']
    mensajeExito = "Grua eliminada exitosamente."

@require_http_methods(["GET"])
def buscar_grua(request):
    queryset = GruaModel.objects.values('id','patente')
    params = dict(request.GET.lists())
    if 'q' in params:
        queryset = queryset.filter(patente__icontains=params['q'][0])
    if 'city' in params :
        queryset = queryset.filter(comuna__id=params['city'][0])
    if 'type' in params:
        if params['type'][0] == 'free':
            queryset = queryset.filter(disponible=True)
    return JsonResponse(list(queryset),safe=False)
from apps.accidents.models import Accident
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from django.views.decorators.http import require_http_methods
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.shortcuts import render
from django.db.models import query
from ..models import TallerModel
from ..forms import TallerForm
from django.contrib.auth.models import User


class ListTallerView(PermisosRequeridos,ListView):
    model = TallerModel
    template_name='taller/listTaller.html'
    permission_required = ['sucursal.view_taller']
    miTaller = False
    # def get_template_names(self):
    #     self.miTaller = User.objects.filter(pk=self.request.user.pk, groups__name__in='Taller').exists()
    #     if not self.request.user.is_superuser and self.miTaller == True: 
    #         self.template_name = 'taller/encargadoTaller.html'
    #     return self.template_name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de talleres registrados'
        context['tituloSeccion'] = context['tituloPagina']
        context['titulo'] = 'talleres'
        context['url'] = reverse_lazy('listTaller')
        miTaller = User.objects.filter(pk=self.request.user.pk, groups__name__in='Taller')
        if miTaller.exists():
            context['object_list'] =  TallerModel.objects.filter(encargado=self.request.user)
        return context

class AddTallerView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = TallerModel
    form_class = TallerForm
    permission_required =   ['sucursal.add_taller']
    success_url = reverse_lazy('listTaller')
    mensajeExito = 'Taller registrado exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar taller'
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['columnaForm'] = True
        context['url'] = reverse_lazy('listTaller')
        return context

class ChangeTallerView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'base/baseForm.html'
    model = TallerModel
    form_class = TallerForm
    permission_required =   ['sucursal.change_taller']
    success_url = reverse_lazy('listTaller')
    mensajeExito = 'Taller actualizado exitosamente'

    def get_initial(self):
        return {'usuario': self.request.user}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar taller' + str(self.object.nombre)
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['botonEnviar'] =  "Actualizar taller"
        context["action"] = "edit"
        context['columnaForm'] = True
        context['url'] = reverse_lazy('listTaller')
        return context

class DeleteTallerView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = TallerModel
    permission_required =   ['sucursal.delete_taller']
    mensajeExito = "Taller eliminado exitosamente."

class ListCarTallerView(PermisosRequeridos,ListView):
    model = Accident
    template_name='taller/listAccidentTaller.html'
    permission_required = ['sucursal.accept_taller']

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'check':
                actual = Accident.objects.get(pk=request.POST['pk'])
                actual.estado = 3
                actual.grua.disponible = True
                actual.grua.save()
                actual.save()
            if action == 'finish':
                Accident.objects.filter(pk=request.POST['pk']).update(estado=6)
        except Exception as e:
            data = {'error': str(e)}
        return JsonResponse(data)


    def get_queryset(self):
        taller = TallerModel.objects.all()
        if not self.request.user.is_superuser:taller = taller.filter(encargado=self.request.user)
        q = self.model.objects.filter(taller__in=taller).order_by('estado')
        return q

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de autos en taller'
        context['tituloSeccion'] = 'Lista de autos en taller'
        context['titulo'] = 'talleres'
        context['url'] = reverse_lazy('listTallerCar')
        return context

# decorador para declarar que solo se permite el metodo get 
@require_http_methods(["GET"])
def buscar_taller(request):
    # se inicializa un queryset donde solo se toma el id y nombre del modelo
    queryset = TallerModel.objects.values('id','nombre')
    # se crea un dicionario del get request para obtener los parametros y tener el codigo un poco mas limpio
    params = dict(request.GET.lists())
    # se valida que en los parametros este presente q
    if 'q' in params:
        # se aplica el filtro correpondiente al modelo usando q y como params['q'] devuelve una lista se toma el primer valor en la posicion [0]
        queryset = queryset.filter(nombre__icontains=params['q'][0])
    # se valida que en los parametros este presente city
    if 'city' in params :
        # se aplica el filtro correpondiente al modelo usando q city como params['city'] devuelve una lista se toma el primer valor en la posicion [0]
        queryset = queryset.filter(comuna__id=params['city'][0])
    # se valida que en los parametros este presente type
    if 'type' in params:
        # se valida que type sea igual a free de esta manera se da a entender que se busca un taller libre
        if params['type'][0] == 'free':
            # se usa el filtro correspondiente al modelo para solo obtener talleres que esten disponibles
            queryset = queryset.filter(reparaciones_disponible=True)
    return JsonResponse(list(queryset),safe=False)
from django.shortcuts import render
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from ..models import SucursalModel
from ..forms import SucursalForm, SucursalModel
from django.urls import reverse_lazy
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo

class ListSucursalView(PermisosRequeridos,ListView):
    model = SucursalModel
    template_name='appSucursal/listSucursal.html'
    permission_required = ['sucursal.view_sucursal']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de sucursales'
        context['titulo'] = 'Sucursales'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listSucursal')
        return context

class AddSucursalView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = SucursalModel
    form_class = SucursalForm
    permission_required =   ['sucursal.add_sucursal']
    success_url = reverse_lazy('listSucursal')
    mensajeExito = 'Sucursal registrada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar sucursal'
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['columnaForm'] = True
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listSucursal')
        return context

class ChangeSucursalView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'base/baseForm.html'
    model = SucursalModel
    form_class = SucursalForm
    permission_required =   ['sucursal.change_sucursal']
    success_url = reverse_lazy('listSucursal')
    mensajeExito = 'Sucursal actualizada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizando sucursal ' + self.object.nombre
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  "Actualizar sucursal"
        context["action"] = "edit"
        context['columnaForm'] = True
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listSucursal')
        return context

class DeleteSucursalView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = SucursalModel
    permission_required =   ['sucursal.delete_sucursal']
    mensajeExito = "Sucursal eliminada exitosamente."
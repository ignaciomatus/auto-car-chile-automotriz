from django.shortcuts import render
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from ..models import ComunaModel
from ..forms import ComunaForm
from django.urls import reverse_lazy
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo

class ListComunaView(PermisosRequeridos,ListView):
    model = ComunaModel
    template_name='appSucursal/listComuna.html'
    permission_required = ['sucursal.view_comuna']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de comunas'
        context['tituloSeccion'] = context['tituloPagina']
        context['titulo'] = 'Comunas'
        context['url'] = reverse_lazy('listComuna')
        return context

class AddComunaView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = ComunaModel
    form_class = ComunaForm
    permission_required =   ['sucursal.add_comuna']
    success_url = reverse_lazy('listComuna')
    mensajeExito = 'Comuna registrada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar comuna'
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['url'] = reverse_lazy('listComuna')
        return context

class ChangeComunaView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'base/baseForm.html'
    model = ComunaModel
    form_class = ComunaForm
    permission_required =   ['sucursal.change_comuna']
    success_url = reverse_lazy('listComuna')
    mensajeExito = 'Comuna actualizada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar comuna' + str(self.object.nombre)
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context['botonEnviar'] =  "Actualizar comuna"
        context["action"] = "edit"
        context['url'] = reverse_lazy('listComuna')
        return context

class DeleteComunaView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = ComunaModel
    permission_required =   ['sucursal.delete_comuna']
    mensajeExito = "Comuna eliminada exitosamente."
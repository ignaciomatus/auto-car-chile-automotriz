from django.forms.models import ModelChoiceField
from django.http import request
from apps.core.forms import IncializarFormulario
from django import forms
from .models import ComunaModel, GruaModel, SucursalModel, TallerModel, User

#nombre
class ComunaForm(IncializarFormulario):
    nombre = forms.CharField(label="Nombre de la Comuna*",widget=forms.TextInput(attrs={'placeholder': 'Nombre de la comuna'}))

    class Meta:
        model = ComunaModel
        fields = ['nombre']
    
    def clean(self):
        nombre = self.cleaned_data['nombre'].lower().strip()
        if ComunaModel.objects.filter(nombre__icontains=nombre).exclude(pk=self.instance.pk).exists():
            self.add_error('nombre', "Ya existe una comuna registrada con este nombre")

class SucursalForm(IncializarFormulario):
    nombre = forms.CharField(label="Nombre de la sucursal*",widget=forms.TextInput(attrs={'placeholder': 'Nombre de la sucursal'}))
    direccion = forms.CharField(label="Dirección de la sucursal*",widget=forms.TextInput(attrs={'placeholder': 'Dirección de la sucursal'}))
    comuna =forms.ModelChoiceField(required=True,label='Seleccionar comuna',
        help_text="Seleccionar comuna a la cual pertenece esta sucursal", queryset=ComunaModel.objects.all())
    nombre = forms.CharField(label="Nombre de la sucursal*",widget=forms.TextInput(attrs={'placeholder': 'Nombre de la sucursal'}))
    rut = forms.CharField(label="RUT asociado a la sucursal*",widget=forms.TextInput(attrs={'placeholder': 'RUT de la sucursal'}))

    class Meta:
        model = SucursalModel
        fields = ['nombre', 'direccion', 'rut', 'comuna']

class TallerForm(IncializarFormulario):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['activo'].widget.attrs['class'] = 'form-check-input'
        if self.instance.pk:
            if 'initial' in kwargs:
                usuario = kwargs['initial']['usuario']
                if not usuario.is_superuser:
                    self.fields['encargado'].widget = forms.HiddenInput()
                    self.fields['activo'].widget = forms.HiddenInput()
                    self.fields['comuna'].widget = forms.HiddenInput()
                    self.fields['reparaciones_maximas'].widget = forms.HiddenInput()
        else:
            self.fields['activo'].initial = True
        
        

    encargado = forms.ModelChoiceField(required=True,label='Seleccionar encargado',
        help_text="Seleccionar usuario encargado de este taller", queryset=User.objects.filter(groups__name__icontains='Taller'))
    nombre = forms.CharField(label="Nombre del taller*",widget=forms.TextInput(attrs={'placeholder': 'Nombre del taller'}))
    direccion = forms.CharField(label="Dirección del taller*",widget=forms.TextInput(attrs={'placeholder': 'Dirección del taller'}))
    comuna =forms.ModelChoiceField(required=True,label='Seleccionar comuna',
        help_text="Seleccionar comuna a la cual pertenece este taller", queryset=ComunaModel.objects.all())
    reparaciones_maximas = forms.IntegerField(label="Reparaciones maximas*",widget=forms.NumberInput(attrs={'placeholder': 'Numero del taller'}),
    help_text = "Cantidad máxima de vehiculos que se le pueden enviar a este taller al mismo tiempo")
    activo = forms.BooleanField(required=False,help_text='Seleccionar solo si el taller esta activo',widget= forms.CheckboxInput())

    class Meta:
        model = TallerModel
        fields = [ 'nombre','encargado', 'direccion', 'comuna', 'reparaciones_maximas', 'activo']

class GruaForm(IncializarFormulario):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.pk:
            self.fields['activo'].initial = True
        self.fields['activo'].widget.attrs['class'] = 'form-check-input'

    marca = forms.CharField(label="Marca*",widget=forms.TextInput(attrs={'placeholder': 'Marca'}))
    modelo = forms.CharField(label="Modelo*",widget=forms.TextInput(attrs={'placeholder': 'Modelo'}))
    patente = forms.CharField(label="Patente*",widget=forms.TextInput(attrs={'placeholder': 'Patente'}))
    anio = forms.IntegerField(label='Año*', widget=forms.NumberInput(attrs={'placeholder':'Año'}))
    comuna =forms.ModelChoiceField(required=True,label='Seleccionar comuna',
        help_text="Seleccionar comuna a la cual pertenece esta grua", queryset=ComunaModel.objects.all())
    activo = forms.BooleanField(required=False,help_text='Seleccionar solo si la grua esta activa',widget= forms.CheckboxInput())
    chofer = forms.ModelChoiceField(required=True,label='Seleccionar chofer',
        help_text="Seleccionar chofer encargado de esta grua", queryset=User.objects.filter(groups__name__icontains='Grua'))

    class Meta:
        model = GruaModel
        fields = ['comuna', 'chofer','marca', 'modelo', 'patente', 'anio',  'activo']
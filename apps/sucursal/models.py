from apps.clients.models import Vehiculo, Cliente
from django.contrib.auth.models import User
from django.db import models

class ComunaModel(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'comuna'
        default_permissions = ()
        permissions = (
            ("view_comuna",     "Ver Comuna"),
            ("add_comuna",      "Registrar Comuna"),
            ("change_comuna",   "Actualizar Comuna"),
            ("delete_comuna",   "Eliminar Comuna"),
        )

class SucursalModel(models.Model):
    nombre = models.CharField(max_length=50)
    comuna = models.ForeignKey(ComunaModel, on_delete=models.PROTECT)
    direccion = models.CharField(max_length=50)
    rut = models.CharField(max_length = 20)
    
    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'sucursal'
        default_permissions = ()
        permissions = (
            ("view_sucursal",     "Ver sucursal"),
            ("add_sucursal",      "Registrar sucursal"),
            ("change_sucursal",   "Actualizar sucursal"),
            ("delete_sucursal",   "Eliminar sucursal"),
        )

class TallerModel(models.Model):
    encargado = models.ForeignKey(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=50)
    activo = models.BooleanField(default=True)
    reparaciones_maximas = models.IntegerField(default=3)
    reparaciones_asignadas = models.IntegerField(default=0)
    reparaciones_disponible = models.BooleanField(default=True)
    comuna = models.ForeignKey(ComunaModel, on_delete=models.PROTECT)

    def __str__(self):
        return self.nombre

    def change_cars_on_work(self,quantity):
        self.reparaciones_asignadas = self.reparaciones_asignadas + quantity
        self.reparaciones_disponible = bool(self.reparaciones_asignadas < self.reparaciones_maximas)
        if self.reparaciones_maximas == 0:
            self.reparaciones_disponible = True
        self.save()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.reparaciones_asignadas < self.reparaciones_maximas: self.reparaciones_disponible = True
        super().save()


    def liberate(self):
        self.change_cars_on_work(-1)

    def occupy(self):
        self.change_cars_on_work(1)

    class Meta:
        db_table = 'taller'
        default_permissions = ()
        permissions = (
            ("view_taller",     "Ver taller"),
            ("add_taller",      "Registrar taller"),
            ("change_taller",   "Actualizar taller"),
            ("delete_taller",   "Eliminar taller"),
            ("accept_taller",   "Aceptar auto en taller"),
            ("cotizacion_taller",   "Crear cotización de la reparación"),
        )

class GruaModel(models.Model):
    chofer = models.ForeignKey(User, on_delete=models.CASCADE)
    marca = models.CharField(max_length=20)
    modelo = models.CharField(max_length=50)
    patente = models.CharField(max_length=20)
    anio = models.CharField(max_length=4)
    comuna = models.ForeignKey(ComunaModel, on_delete=models.PROTECT)
    disponible = models.BooleanField(default=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return "{} {} {}".format(self.marca,self.modelo, self.anio)

    def liberate(self):
        self.disponible = True
        self.save()

    def occupy(self):
        self.disponible = False
        self.save()

    class Meta:
        db_table = 'grua'
        default_permissions = ()
        permissions = (
            ("view_grua",     "Ver grua"),
            ("add_grua",      "Registrar grua"),
            ("change_grua",   "Actualizar grua"),
            ("delete_grua",   "Eliminar grua"),
            ('aceptar_grua',  "Aceptar Asistir a siniestro")
        )
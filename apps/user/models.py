from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from crum import get_current_user
from apps.sucursal.models import SucursalModel

class PerfilUsuario(models.Model):
    usuario = models.OneToOneField(User, related_name="perfilusuario", on_delete=models.CASCADE, null=True, blank=True)
    telefono = models.CharField(max_length = 15)
    sucursal = models.ForeignKey(SucursalModel, on_delete=models.PROTECT, null=True, blank=True)
    siniestros_asignados = models.IntegerField(default=0) #Siniestros asignados en ese momento
    siniestros_maximos = models.IntegerField(default=3) #Maximo de siniestros que se le pueden asignar al usuario
    siniestros_disponible = models.BooleanField(default=True) #Maximo de siniestros disponibles
    usuario_add = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuarioperfil_add', null=True, blank=True)
    fecha_add = models.DateTimeField(auto_now_add=True)
    usuario_change = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuarioperfil_change', null=True, blank=True)
    fecha_change = models.DateTimeField(auto_now=True)  
    #.exclude(Si siniestros asignados == siniestros_maximo) filter(sobre_capacidad=False)
    #Si el siniestro != maximo, y ordernar por < siniestros asignados, sucursal de la misma comuna del accidente

    def __str__(self): 
        return self.usuario.username
    #User.objects.filter(perfilusuario__sucursal__comuna=comunaAccidente, perfilusuario__sucursal__sobre_capacidad = False).order_by('siniestros_asignados')
    # def para validar que no supere el maximo y setear la bandera

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        user = get_current_user()
        if user is not None:
            if not self.pk:self.usuario_add = user
            else:self.usuario_change = user
        super().save()
    
    def change_accidents(self,quantity):
        self.siniestros_asignados = self.siniestros_asignados + quantity
        self.siniestros_disponible = bool(self.siniestros_asignados  < self.siniestros_maximos)
        self.save()


    def liberate(self):
        self.change_accidents(-1)

    def occupy(self):
        self.change_accidents(1)


    class Meta:
        ordering = ['id']
        default_permissions = ()
        # permissions = (
        #     ("view_perfilUsuario",     "Ver perfil de usuario"),
        #     ("add_perfilUsuario",      "Agregar perfil de usuario"),
        #     ("change_perfilUsuario",   "Modificar perfil de usuario"),
        #     ("delete_perfilUsuario",   "Eliminar perfil de usuario"),
        # )

@receiver(post_save, sender=User)
def crearActualizarPerfilUsario(sender, instance, created, **kwargs):
    if created:
        PerfilUsuario.objects.create(usuario=instance)
    instance.perfilusuario.save()
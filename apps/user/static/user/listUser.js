$(function(){
    $(".tablaActual").on('click', '.cambio', function() {
        var url = $(this).attr('data-url');
        var action =  $(this).attr('action');
        var titulo = 'Dar de baja a usuario';  
        var texto = '¿Desea dar de baja al usuario?';
        var logo = 'fas fa-trash-alt';
        if (action == 'activate'){
            titulo = 'Activar usuario';
            texto = '¿Desea dar de alta nuevamente al usuario actual?';
            logo =  'fas fa-undo-alt';
        }
        
        var parametros = {
            'pk' : $(this).attr('datos'),
            'action' : action,
        }
        alertJS(
           logo,
            url,
            titulo,
            texto,
            parametros,
            function(data) {
                location.reload();
            }
        );
    });
})
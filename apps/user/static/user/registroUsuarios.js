$(function() {
    var misPermisos = $('#id_permisos').bootstrapDualListbox({
        filterPlaceHolder: "Filtrar permisos",
        moveSelectedLabel: "Agregar permisos",
        filterTextClear: "Mostrar todo",
        moveAllLabel: "Mover todos",
        nonSelectedListLabel: 'Permisos disponibles',
        selectedListLabel: 'Permisos asignados',
        infotextempty: "Lista vacia",
        infotext: "Lista vacia",
        selectorMinimalHeight: 500,
        preserveSelectionOnMove: 'moved',
        moveOnSelect: true,
    });
    $('select[name="groups"]').on('change', function() {
        ConsultaAjax({
            'action': 'searchPermisos',
            'id': $(this).val(),
        }, function(data) {
            $('#id_permisos').val(data);
            misPermisos.bootstrapDualListbox('refresh');
        }, function(data) {});
    });
});
from django.db.models import query
from apps.user.models import PerfilUsuario
from django.shortcuts import render
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from .forms import UsuarioForm
from django.contrib.auth.views import LoginView

from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth.models import Group
from django.db import transaction
from django.contrib import messages
from django.http import JsonResponse
from django.forms import model_to_dict
from apps.core.mixins import GuardarModelo, StaffPermiso, PermisosRequeridos


class ListUserView(PermisosRequeridos,ListView):
    model = User
    template_name='user/list.html'
    permission_required = ['auth.view_user']

    def obtenerLista(self, lista):
        datos = []
        lista = lista.order_by('-is_active')
        for usuario in lista:
            item = model_to_dict(usuario, exclude=['password', 'user_permissions', 'is_staff','date_joined', 'is_superuser'])   
            item['groups'] = [{'id': g.id, 'name': g.name} for g in usuario.groups.all()]
            item['creador'] = usuario.perfilusuario.usuario_add
            item['sucursal'] = usuario.perfilusuario.sucursal
            item['siniestros_asignados'] = str(usuario.perfilusuario.siniestros_asignados) + "/" +  str(usuario.perfilusuario.siniestros_maximos)
            item['disponible'] = usuario.perfilusuario.siniestros_disponible
            datos.append(item)
        return datos

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de usuarios'
        context['tituloSeccion'] = context['tituloPagina']
        context["listadoUsuarios"] = self.obtenerLista(context["object_list"])
        context['url'] = reverse_lazy('listUser')
        return context

class BuscadorPermisos(object):
    def buscadorPOST(self,action):
        data = {}
        if action == "searchPermisos" and self.request.POST["id"] != "":
            grupo = Group.objects.get(pk=self.request.POST["id"])
            permisos = grupo.permissions.all()
            data = [val.id for val in permisos]
        return data

class AddUserView(PermisosRequeridos,GuardarModelo, BuscadorPermisos, FormView):
    template_name = 'user/register.html'
    form_class = UsuarioForm
    success_url = reverse_lazy('listUser')
    permission_required = ['auth.add_user']
    mensajeExito = 'Usuario registrado exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar usuario'
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = titulo
        context["action"] = "add"
        context['botonEnviar'] = titulo
        context['url'] = reverse_lazy('listUser')
        return context

class ChangeUserView(PermisosRequeridos,GuardarModelo, BuscadorPermisos, UpdateView):
    model = User
    template_name = 'user/register.html'
    form_class = UsuarioForm
    permission_required = ['auth.change_user']
    success_url = reverse_lazy('listUser')
    mensajeExito = 'Usuario actualizado exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar usuario'
        context['tituloPagina'] = titulo
        context['tituloSeccion'] = context['tituloPagina']
        context['tituloForm'] = 'Actualizar usuario ' + str(self.object.username)
        context["action"] = "edit"
        context['botonEnviar'] = titulo
        context['url'] = reverse_lazy('listUser')
        return context

class DeleteUserView(PermisosRequeridos,DeleteView):
    model = User
    permission_required =   ['auth.delete_user']
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            self.object = self.get_object() 
            if action == 'delete':
                self.object.is_active = 0
                self.object.save()
                messages.success(request, "Usuario dado de baja exitosamente.")
            elif action == 'activate':
                self.object.is_active = 1
                self.object.save()
                messages.success(request, "Usuario activado exitosamente.")
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

class LoginUserView(LoginView):
    template_name = "user/loginForm.html"

def buscar_liquidador(request):
    queryset = PerfilUsuario.objects.values('usuario__id','usuario__first_name')
    params = dict(request.GET.lists())
    if 'q' in params:
        queryset = queryset.filter(usuario__first_name__icontains=params['q'][0])
    if 'city' in params :
        queryset = queryset.filter(sucursal__comuna__id=params['city'][0])
    if 'type' in params:
        if params['type'][0] == 'free':
            queryset = queryset.filter(siniestros_disponible=True).filter(usuario__groups__name__icontains='Liquidador')
    queryset = queryset.order_by('-siniestros_asignados')
    return JsonResponse(list(queryset),safe=False)
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.contrib.auth.models import Permission, Group
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import password_validation
from apps.sucursal.models import SucursalModel


import re

def validarTelefono(telefono):
    patron = "^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$"
    if not (re.search(patron,telefono)): raise ValidationError("Este telefono no esta en formato correcto de 10 digitos (opcional codigo del pais)")
    return telefono


class IncializarFormulario(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control select2' if form.field.widget.__class__.__name__ == 'Select' else 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
    
    

class UsuariosPermisos(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        #print(obj.codename, obj.content_type.app_label)
        if obj.content_type.app_label in ["admin","contenttypes","authtoken", "sessions", "admin_interface", "permission"]  : pass
        elif obj.content_type.app_label == "auth":
            if(obj.codename == "add_group"): obj = "Grupos - Agregar grupos"
            elif(obj.codename == "change_group"): obj = "Grupos - Modificar grupos"
            elif(obj.codename == "delete_group"): obj = "Grupos - Eliminar grupos"
            elif(obj.codename == "view_group"): obj = "Grupos - Ver grupos"
            elif(obj.codename == "add_user"): obj = "Usuarios - Agregar Usuarios"
            elif(obj.codename == "change_user"): obj = "Usuarios - Modificar usuarios"
            elif(obj.codename == "delete_user"): obj = "Usuarios - Eliminar usuarios"
            elif(obj.codename == "view_user"): obj = "Usuarios - Ver usuarios"
            elif(obj.codename == "add_permission"): obj = "Permisos - Agregar permisos"
            elif(obj.codename == "change_permission"): obj = "Permisos - Modificar permisos"
            elif(obj.codename == "delete_permission"): obj = "Permisos - Eliminar permisos"
            elif(obj.codename == "view_permission"): obj = "Permisos - Ver permisos"
        else:
            #print(str(obj.content_type))
            obj = str(obj.content_type.name.capitalize().replace('model','')) + " - " + str(obj.name.capitalize() )
            
        return obj

from django.contrib.admin.widgets import FilteredSelectMultiple

class UsuarioForm(IncializarFormulario):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['is_active'].initial = True
        self.fields['is_active'].widget.attrs['class'] = 'form-check-input'

        self.fields['username'].widget.attrs['autofocus'] = True
        if self.instance.pk:
            #print("hola mundo", self.instance.pk, self.instance.perfilusuario.telefono)
            grupo = Group.objects.filter(user = self.instance)
            permisos = Permission.objects.filter(user=self.instance)
            if grupo.exists(): self.initial['groups'] = grupo.first().pk
            self.initial['permisos'] = permisos
            self.fields['password1'].required = False
            self.fields['password2'].required = False
            self.fields['telefono'].initial = self.instance.perfilusuario.telefono
            self.fields['sucursal'].initial = self.instance.perfilusuario.sucursal
            self.fields['siniestros_maximos'].initial = self.instance.perfilusuario.siniestros_maximos
        else:
            self.fields['siniestros_maximos'].initial = 0

    username = forms.CharField(label="Nombre de usuario*",widget=forms.TextInput(attrs={'placeholder': 'Nombre de usuario'}))
    email = forms.EmailField(required=False, label="Correo electronico*",widget=forms.EmailInput(attrs={'placeholder': 'Ingrese su correo electronico'}))
    first_name = forms.CharField(label="Nombre*",widget=forms.TextInput(attrs={'placeholder': 'Nombre (s)'}))
    last_name = forms.CharField(label="Apellido*",widget=forms.TextInput(attrs={'placeholder': 'Apellido (s)'}))
    telefono = forms.CharField(label="Numero de telefono*",widget=forms.TextInput(attrs={'placeholder': 'Ingresar numero telefonico','maxlength': '15'}),
    validators=[validarTelefono])
    password1 = forms.CharField(label="Contraseña*", widget=forms.PasswordInput(attrs={'placeholder':'Ingresar contraseña'}))
    password2 = forms.CharField(label="Repetir contraseña*", widget=forms.PasswordInput(attrs={'placeholder':'Repetir contraseña'}))
    groups =  forms.ModelChoiceField(required=True,label="Grupo del usuario", help_text="Seleccionar el grupo al que desea que pertenezca el usuario para asignarle los permisos correspondientes",
         queryset=Group.objects.all() )
    permisos = UsuariosPermisos(required=False,queryset=Permission.objects.all().exclude(content_type__app_label__in=["admin","contenttypes","sessions", "authtoken", "admin_interface", "permission"]).order_by("content_type__model"),#.order_by("content_type__name"),
        widget=forms.SelectMultiple(attrs={'class': 'form-control multiple','style': 'width: 100%'}))
    sucursal =forms.ModelChoiceField(required=False,label='Seleccionar sucursal de usuario',
        help_text="Seleccionar sucursal a la cual tendra acceso el usuario en caso de ser un Call Center o trabajador interno. Caso contrario, dejar vacio.", queryset=SucursalModel.objects.all())
    siniestros_maximos = forms.IntegerField(label="Siniestros maximos", widget=forms.NumberInput(), 
        help_text="Número maximo de siniestros que puede atender al mismo tiempo en caso de ser un liquidador. Caso contrario, dejar en 0")
    
    is_active = forms.CheckboxInput()

    class Meta:
        model = User
        fields = ('username', 'email', 'password1' ,'password2' , 'first_name','last_name', 'telefono', 'siniestros_maximos', 'sucursal', 'groups', 'is_active', 'permisos' )

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                #print("Entra a validar", self.cleaned_data['groups'],self.cleaned_data['permisos'], self.cleaned_data['scu'])
                clave = self.cleaned_data['password1']
                permisos = Permission.objects.filter(pk__in=self.cleaned_data['permisos'])
                usuario = form.save(commit=False)
                if not usuario.pk is None:
                    if usuario.password != clave and clave != "": 
                        usuario.set_password(clave)
                else:
                    usuario.set_password(clave)
                usuario.save()
                usuario.perfilusuario.telefono = self.cleaned_data['telefono']
                usuario.perfilusuario.sucursal = self.cleaned_data['sucursal']  
                usuario.perfilusuario.siniestros_maximos = self.cleaned_data['siniestros_maximos']
                usuario.user_permissions.set(permisos)
                usuario.groups.clear()
                usuario.groups.add(Group.objects.get(name=self.cleaned_data['groups'])) 
                usuario.save()
                return usuario
            else:
                data['error'] = form.errors
        except Exception as e:
            print(str(e))
            data['error'] = str(e)
        return data


    class Media:
            css = {
                'all': ('/static/admin/css/widgets.css',),
            }
            js = ('/admin/jsi18n',)

    error_messages = {
        'password_mismatch': 'Las contraseñas no coinciden',
    }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except ValidationError as error:
                self.add_error('password2', error)

    def clean(self):
        username = self.cleaned_data['username'].lower()
        email = self.cleaned_data['email'].lower()
        usuario = User.objects.all().exclude(pk=self.instance.pk)
        if usuario.filter(username=username).exists():
            self.add_error('username', "Ya existe un usuario registrado con este nombre de usuario. Favor de registrar otro")
        if usuario.filter(email__icontains=email).exists() and email !="":
            self.add_error('email', "Ya existe un usuario registrado con este correo electronico. Favor de registrar otro")

class ProfileForm(IncializarFormulario):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['autofocus'] = True
        if self.instance.pk:
            self.initial['permisos'] = self.instance.permissions.all()

    name = forms.CharField(label="Nombre del grupo*",required=True,widget=forms.TextInput(attrs={'placeholder': 'Nombre del grupo de usuario'}))
    permisos = UsuariosPermisos(label ="",required=True,queryset=Permission.objects.all().exclude(content_type__app_label__in=["admin","contenttypes","sessions", "authtoken", "admin_interface", "permission"]).order_by("content_type__model"),#.order_by("content_type__name"),
        widget=forms.SelectMultiple(attrs={'class': 'form-control multiple','style': 'width: 100%'}))

    class Meta:
        model = Group
        fields = ['name', 'permisos']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                permisos = Permission.objects.filter(pk__in=self.cleaned_data['permisos'])
                grupo = form.save()
                #grupo.permissions.clear()
                grupo.permissions.set(permisos)
                grupo.save()
                return grupo
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data


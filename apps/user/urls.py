from django.urls import path, include
from .views import ListUserView, AddUserView, ChangeUserView, DeleteUserView, LoginUserView, buscar_liquidador
from .viewsPermisos import ListProfileView, AddProfileView, ChangeProfileView, DeleteProfileView
from django.contrib.auth.views import LogoutView
urlpatterns = [
    path('list', ListUserView.as_view(), name="listUser"),
    path('register', AddUserView.as_view(), name="addUser"),
    path('change/<int:pk>/',ChangeUserView.as_view(),name = 'changeUser'),
    path('delete/<int:pk>/',DeleteUserView.as_view(),name = 'deleteUser'),

    path('login/', LoginUserView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),

    path('profile/list', ListProfileView.as_view(), name="listProfile"),
    path('profile/register', AddProfileView.as_view(), name="addProfile"),
    path('profile/change/<int:pk>/',ChangeProfileView.as_view(),name = 'changeProfile'),
    path('profile/delete/<int:pk>/',DeleteProfileView.as_view(),name = 'deleteProfile'),

    path('liquidador/buscar',buscar_liquidador,name = 'buscarLiquidador'),
]

from django.shortcuts import render
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo
from django.contrib.auth.models import Group, User

from django.http import JsonResponse
from django.contrib import messages
from .forms import ProfileForm
from django.db import transaction
from django.urls import reverse_lazy


class ListProfileView(PermisosRequeridos, ListView):
    model = Group
    template_name='profile/list.html'
    permission_required = ['auth.view_group']

    def get_queryset(self):
        queryset =  self.model.objects.all()
        usuarios = User.objects.all()
        for val in queryset:
            val.permisos = val.permissions.count()
            val.usuarios = usuarios.filter(groups=val).count()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de grupos de usuario'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listProfile')
        return context

class AddProfileView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'profile/register.html'
    model = Group
    form_class = ProfileForm
    permission_required =   ['auth.add_group']
    success_url = reverse_lazy('listProfile')
    mensajeExito = "Grupo de usuario con permisos creado exitosamente"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Registrar grupo de usuario'
        context['tituloForm'] = 'Registrar grupo de usuario'
        context["action"] = "add"
        context['botonEnviar'] = 'Registrar grupo'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listProfile')
        return context

class ChangeProfileView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'profile/register.html'
    model = Group
    form_class = ProfileForm
    permission_required =   ['auth.change_group']
    success_url = reverse_lazy('listProfile')
    mensajeExito = "Grupo de usuario con permisos actualizado exitosamente"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Actualizar grupo de usuario "{}"'.format(self.object.name)
        context['tituloForm'] = 'Actualizar grupo de usuario "{}"'.format(self.object.name)
        context["action"] = "edit"
        context['botonEnviar'] = 'Actualizar grupo'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listProfile')
        return context

class DeleteProfileView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = Group
    permission_required =   ['auth.delete_group']
    mensajeExito = "Grupo eliminado exitosamente."
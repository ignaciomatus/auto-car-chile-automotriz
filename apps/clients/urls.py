from django.urls import path
from .views import denuncia, poliza, siniestro, vehiculo, finalizar,valid_policy

urlpatterns = [
    #path('', index, name="index"),
    #path('contact/', contact, name="contact"),
    #path('registro/', registro, name="registro"),
    path('denuncia/', denuncia, name="denuncia"),
    path('poliza/', poliza, name="poliza"),
    path('siniestro/', siniestro, name="siniestro"),
    path('vehiculo/', vehiculo, name="vehiculo"),
    path('finalizar/', finalizar, name="finalizar"),   
    path('valid_policy/', valid_policy, name="valid_policy"),    
]


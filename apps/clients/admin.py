from django.contrib import admin
from .models import Cliente, Consulta_poliza, Vehiculo,Poliza

# Register your models here.


class ClienteAdmin(admin.ModelAdmin):
    list_display = ["rut","nombre","apellido","nro_doc","email","telefono","direccion"]
    list_editable = ["email","direccion"] 
    search_fields = ["rut"]
    list_per_page = 1


admin.site.register(Cliente, ClienteAdmin)

#------------------------------------------------------------------------------------------------------

class Consulta_poliza_Admin(admin.ModelAdmin):
    list_display = ["rut","poliza"]
    search_fields = ["rut"]
    list_per_page = 1

admin.site.register(Consulta_poliza, Consulta_poliza_Admin)

#------------------------------------------------------------------------------------------------------

# class form_contacto_Admin(admin.ModelAdmin):
#     list_display = ["nombre","apellido","telefono","correo","mensaje"]
#     search_fields = ["nombre"]
#     list_per_page = 1

# admin.site.register(form_contacto, form_contacto_Admin)

#------------------------------------------------------------------------------------------------------

class VehiculoAdmin(admin.ModelAdmin):
    list_display = ["patente","marca","modelo","year","nuevo"]
    search_fields = ["patente"]
    list_per_page = 1

admin.site.register(Vehiculo, VehiculoAdmin)

admin.site.register(Poliza)
from django.contrib.auth.decorators import login_required
from .forms import ContactoForm, CustomUserCreationForm, PolizaForm, VehiculoForm
from apps.core.decorators import group_required,superuser_required,permission_required
from .models import Cliente, Consulta_poliza, Vehiculo,Poliza
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.http import JsonResponse, response
from apps.accidents.models import Accident
from django.contrib import messages


# Create your views here.

# def index(request):
#     return render(request, 'app/index.html')

def denuncia(request):
    return render(request, 'app/denuncia.html')

def finalizar(request):
    return render(request, 'app/finalizar.html')

def vehiculo(request):
    data = {
        'form': VehiculoForm()
    }

    if request.method == 'POST':
        formulario = VehiculoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Datos Guardados Correctamente"
            return redirect(to='finalizar')

        else:
            data["form"] = formulario


    return render(request, 'app/vehiculo.html', data)

# @permission_required('clients.view_poliza')
@require_http_methods(["GET"])
def poliza(request):
    return render(request, 'app/poliza.html', {
        'tituloPagina' : 'Buscar polizas',
        'tituloSeccion' : 'Buscar polizas',
    })

def siniestro(request):
    data = {
        'form': ContactoForm()
    }

    if request.method == 'POST':
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Mensaje Enviado"
            return redirect(to='vehiculo')

        else:
            data["form"] = formulario

    return render(request, 'app/siniestro.html', data)

# decorador para aceptar solo GET
# @permission_required('clients.view_poliza')
@require_http_methods(["GET"])
def valid_policy(request):
    # se inicializa la respuesta
    response = {}
    # Poliza
    # numero_poliza
    rut = request.GET.get("rut",None)
    # verificar que exista tanto cliente como codigo de poliza
    # si alguno de los dos es nullo(None) entonces la condicion no se cumple
    if rut:
        policies = Poliza.objects.filter(cliente__rut=rut)
        results = []
        for policy in policies:
            datos = policy.to_json()
            datos['vehiculo']['nuevo'] = "SI" if datos['vehiculo']['nuevo'] == True else "NO"
            last_accident = Accident.objects.exclude(estado=6).filter(poliza=policy).last()
            if last_accident:
                datos['siniestro']=last_accident.to_json()
            results.append(datos)
        response = results
    return JsonResponse(response,safe=False)
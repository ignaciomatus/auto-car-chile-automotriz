from django.forms import model_to_dict
from django.db import models
import uuid

from django.db.models.fields.related import ForeignKey

# Create your models here.

class Cliente(models.Model):
    rut = models.CharField(max_length=14,unique=True)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField(default=None,null=False)
    nro_doc = models.CharField(max_length=14)
    email = models.EmailField()
    telefono = models.CharField(max_length=14)
    direccion = models.CharField(max_length=50)
    # deuda_total = models.DecimalField(max_digits=10,decimal_places=2,default=0)
    
    def __str__(self):
        return self.rut

    def nombreCompleto(self):
        return self.nombre + " " + self.apellido

    class Meta:
        db_table = 'Cliente'
        default_permissions = ()
        permissions = (
            ("view_cliente",     "Ver cliente"),
            ("add_cliente",      "Registrar cliente"),
            ("change_cliente",   "Actualizar cliente"),
            ("delete_cliente",   "Eliminar cliente"),
        )


    




#------------------------------------------------------------------------------------------

class Consulta_poliza(models.Model):
    rut = models.CharField(max_length=14)
    poliza = models.CharField(max_length=10)

    def __str__(self):
        return self.rut

    class Meta:
        db_table = 'consulta_poliza'
        default_permissions = ()
        permissions = (
            ("view_consultarPoliza",     "Consultar poliza con RUT"),
        )
        
#--------------------------------------------------------------------------------------------

opciones_marcas = [
    [0, "Chevrolet"],
    [1, "Hyundai"],
    [2, "Toyota"],
    [3, "Peugeot"],
    [4, "Nissan"],
    [5, "Kia"],
    [6, "Mazda"],
    [7, "Koenigsegg"],
    [8, "Mitsubishi"],
    [9, "Chrysler"],
    [10, "Ford"],
    [11, "Chevrolet"],
    [12, "Dodge"],
]

class Vehiculo(models.Model):
    patente = models.CharField(max_length=10)
    marca = models.IntegerField(choices=opciones_marcas)
    modelo = models.CharField(max_length=50)
    year = models.CharField(max_length=4,verbose_name="año",default=None,null=True)
    nuevo = models.BooleanField(null=True)
    cliente = models.ForeignKey(Cliente,related_name="dueno_auto",on_delete=models.CASCADE)
    tipo = models.CharField(max_length=50,default=None,null=True)


    def __str__(self):
        return self.patente

    class Meta:
        db_table = 'vehiculo'
        default_permissions = ()
        permissions = (
            ("view_vehiculo",     "Ver vehiculo"),
            ("add_vehiculo",      "Registrar vehiculo"),
            ("change_vehiculo",   "Actualizar vehiculo"),
            ("delete_vehiculo",   "Eliminar vehiculo"),
        )


class Poliza(models.Model):
    numero_poliza = models.IntegerField(default=None,null=False,unique=True)
    cantidad_maxima = models.DecimalField(max_digits=10,decimal_places=2,default=1000)
    detalles = models.TextField(default=None)
    cliente = models.ForeignKey(Cliente,related_name="dueno_poliza",on_delete=models.CASCADE)
    vehiculo = models.ForeignKey(Vehiculo,related_name="vehiculo_asociado",on_delete=models.CASCADE)

    def __str__(self) -> str:
        return str(self.numero_poliza)
    
    def to_json(self):
        data = model_to_dict(self,exclude=('cliente','vehiculo',))
        data["cliente"] = model_to_dict(self.cliente)
        data["vehiculo"] = model_to_dict(self.vehiculo)
        return data

    class Meta:
        db_table = 'poliza'
        default_permissions = ()
        permissions = (
            ("view_poliza",     "Ver poliza"),
            ("add_poliza",      "Registrar poliza"),
            ("change_poliza",   "Actualizar poliza"),
            ("delete_poliza",   "Eliminar poliza"),
        )
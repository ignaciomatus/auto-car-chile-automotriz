from django import forms
from .models import Cliente, Consulta_poliza, Vehiculo
from django.contrib.auth.forms import UserCreationForm
from django.forms import ValidationError



class ContactoForm(forms.ModelForm):

    def clean_rut(self):
        rut = self.cleaned_data["rut"]
        existe = Cliente.objects.filter(rut__iexact=rut).exists()

        if existe:
            raise ValidationError("El rut ingresado ya existe!")

        return rut

    class Meta:
        model = Cliente
        fields = '__all__'

class CustomUserCreationForm(UserCreationForm):
    pass
 

class PolizaForm(forms.ModelForm):

    def clean_poliza(self):
        poliza = self.cleaned_data["poliza"]
        existe = Consulta_poliza.objects.filter(poliza__iexact=poliza).exists()

        if existe:
            return poliza
        else:
            raise ValidationError("Póliza vigente!")

        return poliza

    class Meta:
        model = Consulta_poliza
        fields = '__all__'
    
    
# class form_contactoForm(forms.ModelForm): 

#     class Meta:
#         model = form_contacto
#         fields = '__all__'

class VehiculoForm(forms.ModelForm): 
    def clean_patente(self):
        patente = self.cleaned_data["patente"]
        existe = Vehiculo.objects.filter(patente__iexact=patente).exists()

        if existe:
            raise ValidationError("La patente ingresada ya existe!")

        return patente

    class Meta:
        model = Vehiculo
        fields = '__all__'

    
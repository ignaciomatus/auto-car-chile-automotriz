$(function(){
    $(".tablaActual").on('click', '.cambio', function() {
        var url = $(this).attr('data-url');
        var action =  $(this).attr('action');
        var titulo = 'Leer notificación';  
        var texto = '¿Desea marcar el mensaje como leido?';
        var logo = 'fas fa-check-circle';
        if (action == 'borrar'){
            titulo = 'Eliminar registro';
            texto = '¿Estas seguro que deseas eliminar el mensaje?';
            logo =  'fas fa-trash-alt';
        }else if (action == 'nuevo'){
            titulo = 'Marcar como no leido';
            texto = '¿Desea marcar el mensaje como no leido?';
        }
        
        var parametros = {
            'pk' : $(this).attr('datos'),
            'action' : action,
        }
        alertJS(
           logo,
            url,
            titulo,
            texto,
            parametros,
            function(data) {
                location.reload();
            }
        );
    });
})
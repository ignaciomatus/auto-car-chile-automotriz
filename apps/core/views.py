from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, ListView, UpdateView
from .models import ContactoModel
from .forms import ContactoForm
from django.contrib import messages
from django.http import JsonResponse
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo


from apps.accidents.models import Accident
from apps.reparacion.models import ReparacionModel, TipoReparacionModel
from django.contrib.sites.shortcuts import get_current_site

class IndexView(TemplateView):
    template_name = "index.html"
    #template_name = 'correos/presupuesto.html'
    def get_template_names(self):
        if self.request.user.is_authenticated:
            self.template_name = 'indexDash.html'
        return self.template_name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Inicio'
        context['tituloSeccion'] = context['tituloPagina']

        #Prueba de template de correo
        # accidente = Accident.objects.filter(pk=5).first()
        # context["accidente"] = accidente
        # context['cotizacion'] = ReparacionModel.objects.filter(siniestro=accidente).first()
        # context['dominio'] =  get_current_site(self.request).domain if self.request else '127.0.0.1:8000'
        return context

class ContactView(GuardarModelo, FormView):
    form_class = ContactoForm
    model = ContactoModel
    template_name = 'core/contact.html'
    success_url = reverse_lazy('index')
    mensajeExito = "Datos de contacto enviados exitosamente"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Contactanos'
        context['tituloSeccion'] = 'Contacto'
        context['action'] = 'add'
        context['imagenPage'] = True
        return context

class ListContactMessagesView(PermisosRequeridos,ListView):
    model = ContactoModel
    template_name='core/contactList.html'
    permission_required = ['core.view_contactosMensajes']

    def get_queryset(self):
        return self.model.objects.all().order_by('leido')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Listado de mensajes'
        context['tituloSeccion'] = context['tituloPagina']
        context['titulo'] = 'Mensajes'
        return context


class ChangeContactView(PermisosRequeridos,UpdateView):
    model = ContactoModel
    permission_required = ['core.contactosMensajes','core.contactosMensajes']
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'leer':
                self.object.leido = True
                self.object.save()
                messages.success(request, "Mensaje leido exitosamente.")
            elif action == 'nuevo':
                self.object.leido = False
                self.object.save()
                messages.success(request, "Mensaje marcado como no leido.")
            elif action == 'borrar':
                self.object.delete()
                messages.success(request, "Mensaje eliminado exitosamente.")
            else:
                return JsonResponse({'error': 'Sin opción disponible'})
        except Exception as e:
            data = {'error' : str(e)}
        return JsonResponse(data)
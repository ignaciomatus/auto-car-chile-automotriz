from django.contrib.auth.decorators import user_passes_test

# definiendo decorador para validar si un usuario esta en un grupo
def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    # se define funcion que sera utilizada dentro del user_passes_test
    def in_groups(u):
        # se valida que el usuario esta autenticado
        if u.is_authenticated:
            # se verifica que el usuario este dentro de un o mas grupos usando el nombre
            # o si el usuario es superusuario
            return bool(u.groups.filter(name__in=group_names)) | u.is_superuser
        # caso contrario no se devuelve falso
        return False
    
    # se utiliza la funcion user_passes_test para usar el decorador
    # se le pasa la funcion antes definida
    # se le pasa la url para redirecionar en caso de retornar falso
    return user_passes_test(in_groups, login_url='/')

# definiendo decorador para validar el si el usuario es super usuario
def superuser_required():
    """Requires superuser passed in."""
    # se define funcion que sera utilizada dentro del user_passes_test
    def is_superuser(u):
        # se valida que el usuario esta autenticado
        if u.is_authenticated:
            # retorna un boleano de la proiedad del usuario para saber si es superusuario
            return u.is_superuser
        # si el usuario no esta autenticado devuelve falso
        return False
    # se utilliza la funcion user_passes_test para usar el decorador
    # el primer parametro es la funcion antes definida
    # se le pasa la url para redirecionar en caso de retornar falso
    return user_passes_test(is_superuser,login_url='/')

# definiendo decorador para validar los permisos 
def permission_required(*perms):
    """Requires superuser passed in."""
    # se define funcion que sera utilizada dentro del user_passes_test
    def has_perms(u):
        # se valida que el usuario esta autenticado
        if u.is_authenticated:
            # se verifica si el usuario tiene los permisos
            # o si el usuario es superusuario
            return u.has_perms(perms) | u.is_superuser
        # caso contrario no se devuelve falso
        return False
    # se utilliza la funcion user_passes_test para usar el decorador
    # el primer parametro es la funcion antes definida
    # se le pasa la url para redirecionar en caso de retornar falso
    return user_passes_test(has_perms,login_url='/')

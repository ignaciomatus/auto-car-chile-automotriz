from django import forms
from .models import ContactoModel
class IncializarFormulario(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control select2' if form.field.widget.__class__.__name__ == 'Select' else 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

class ContactoForm(IncializarFormulario):
    class Meta:
        model =  ContactoModel
        fields = '__all__'
        exclude = ('leido',)
        widgets = { 
            'nombre': forms.TextInput(attrs={'placeholder': 'Nombre'}),
            'apellido' : forms.TextInput(attrs={'placeholder': 'Apellido (s)'}),
            'telefono' : forms.TextInput(attrs={'placeholder': 'Número telefonico'}),
            'correo' : forms.EmailInput(attrs={'placeholder': 'Correo electronico'}),
            'mensaje' : forms.Textarea(attrs={'placeholder': '¿Cuales son tus dudas? '}),
        }

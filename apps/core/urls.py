from django.urls import path, include
from .views import IndexView, ContactView, ListContactMessagesView, ChangeContactView
urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('contact', ContactView.as_view(), name="contact"),
    path('contact/list/messages', ListContactMessagesView.as_view(), name="messagesContact"),
    path('change/<int:pk>/',ChangeContactView.as_view(),name = 'changeContact'),
    
]

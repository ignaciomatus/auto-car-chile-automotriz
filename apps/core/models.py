from django.db import models

class ContactoModel(models.Model):
    nombre = models.CharField(max_length=50,null=True)
    apellido = models.CharField(max_length=50,null=True)
    telefono = models.CharField(max_length=50,null=True)
    correo = models.EmailField(null=False)
    mensaje = models.TextField(null=False)
    leido = models.BooleanField(default=False)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'contacto'
        default_permissions = ()
        permissions = (
            ("view_contactosMensajes",     "Ver Mensajes de contactanos"),
            ("add_contactosMensajes",      "Registrar Mensajes de contactanos"),
            ("change_contactosMensajes",   "Actualizar Mensajes de contactanos"),
            ("delete_contactosMensajes",   "Eliminar Mensajes de contactanos"),
        )

    
from apps.reparacion.models import ReparacionModel
from django.urls import reverse_lazy
from django import template

register = template.Library()

@register.filter
def get_index(d, key):
    return d[key]

@register.simple_tag
def create_list(*args):
    return args

@register.filter
def get_element(d, **keys):
    value = None
    for key in keys:
        value = d[key]
    return value

@register.filter
def get_status(d, key):
    return d[key][1]

def get_a_template(href,class_color="btn-warning",icon="fa-eye",text=""):
    return '<a href="'+href+'" class="btn '+class_color+' btn-xs btn-flat"><i class="fas '+icon+'"></i> '+text+'</a>'

def user_in_group(user,group_name):
    if user.is_authenticated:
        return bool(user.groups.filter(name__icontains=group_name)) | user.is_superuser
    return False

def get_last_fix(accident):
    fix = ReparacionModel.objects.filter(siniestro=accident)
    if fix.exists(): return fix.last().pk
    return 0

@register.simple_tag
def get_accidents_links(accident,user,perms):
    buttons = ""
    if accident.estado == 0 and perms.__contains__('accidents.asign_accidente'):
        if  user_in_group(user,'analista') and not accident.liquidador:
            title_tmp = 'Cambiar liquidador' if accident.liquidador else 'Asignar Liquidador'
            tmp_url = str(reverse_lazy('accidents:assing',kwargs={'pk': accident.pk}))
            buttons += get_a_template(href=tmp_url,icon="fa-user",text=title_tmp)
        elif user_in_group(user,'liquidador'):
            if not accident.taller or not accident.grua:
                tmp_url = str(reverse_lazy('accidents:assing_workshop',kwargs={'pk': accident.pk}))
                buttons += get_a_template(href=tmp_url,icon="fa-warehouse",text="Asignar Taller/Grua")
            # elif accident.taller and not accident.grua:
            #     tmp_url = str(reverse_lazy('accidents:assing_crane',kwargs={'pk': accident.pk}))
            #     buttons += get_a_template(href=tmp_url,icon="fa-truck-pickup",text="Asignar Grua")
    if accident.estado == 2:
        if perms.__contains__('sucursal.accept_taller') and accident.estado < 3:
            tmp_url = str(reverse_lazy('listTallerCar'))
            buttons += '<a type="button" data-url="'+tmp_url+'" datos="'+str(accident.pk)+'" class="btn btn-success btn-xs btn-flat delete cambio" action="check"> <i class="fas fa-check"></i> Recibir</a>'

    if accident.estado == 5:
        if perms.__contains__('reparacion.change_reparacion'):
            tmp_url = str(reverse_lazy('listTallerCar'))
            buttons += '<a type="button" data-url="'+tmp_url+'" datos="'+str(accident.pk)+'" class="btn btn-success btn-xs btn-flat finish" action="finish"><i class="fas fa-check"></i>Finalizar</a>'

    if perms.__contains__('sucursal.cotizacion_taller') and accident.estado == 3 :
        tmp_url = str(reverse_lazy('addReparacion',kwargs={'pk': accident.pk}))
        buttons += get_a_template(href=tmp_url,icon="fa-money-bill",text="Cotizar")
    
    if perms.__contains__('reparacion.accept_reparacion') and accident.estado == 4 :
        tmp_url = str(reverse_lazy('changeReparacion',kwargs={'pk': get_last_fix(accident.pk)}))
        buttons += get_a_template(href=tmp_url,icon="fa-money-bill",text="Validar Presupuesto")

    tmp_url = str(reverse_lazy('accidents:view_accident',kwargs={'pk': accident.pk}))
    buttons += get_a_template(href=tmp_url)

    return buttons

# https://docs.djangoproject.com/es/2.2/_modules/django/contrib/auth/context_processors/
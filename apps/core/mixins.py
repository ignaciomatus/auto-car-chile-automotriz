from datetime import datetime
from django.contrib import messages
from django.http import HttpResponseRedirect,JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Permission

class Generico:
    permission_required = ''
    url_redirect = None

    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('index')
        return self.url_redirect


from django.conf import settings
from django.core.mail import EmailMultiAlternatives



#import threading

#thread = threading.Thread(target=send_user_mail, args(user, ))
#thead.start()
from django.template.loader import render_to_string

class PermisosRequeridos(LoginRequiredMixin, Generico, object):
    def dispatch(self, request, *args, **kwargs):
        try:self.object = self.get_object()
        except: pass
        if not request.user.has_perms(self.permission_required):
            if request.user.is_authenticated: messages.error(request, 'No tiene permiso para ingresar a este módulo')
            return HttpResponseRedirect(self.get_url_redirect())
        return super().dispatch(request, *args, **kwargs)


class StaffPermiso(LoginRequiredMixin, Generico, object):
    def dispatch(self, request, *args, **kwargs):
        try:self.object = self.get_object()
        except: pass
        if not self.request.user.is_superuser:
            if request.user.is_authenticated: messages.error(request, 'No tiene permiso para ingresar a este módulo')
            return HttpResponseRedirect(self.get_url_redirect())
        return super().dispatch(request, *args, **kwargs)

from django.db import transaction
from django.contrib import messages

class GuardarModelo(object):
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add' or action == 'edit':
                with transaction.atomic():
                    if action == 'edit': form = self.form_class(request.POST, instance=self.get_object())
                    else: form = self.get_form()
                    if form.is_valid():
                        datos = form.save()
                        if( isinstance(datos, dict)): return JsonResponse(datos, safe=False)
                        messages.success(request,self.mensajeExito)
                    else:
                        transaction.set_rollback(True)
                        return JsonResponse( {"error":form.errors} )
            elif callable(getattr(self, "buscadorPOST", None)):
                data = self.buscadorPOST(action)
            else:
                data = {'error': 'No ha ingresado a ninguna opción'}
        except Exception as e:
            data = {"error": str(e)}
        return JsonResponse(data, safe=False)

class BorrarModelo(object):
    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
            messages.success(request, self.mensajeExito)
        except Exception as e:
            data = {'error' : str(e)}
        return JsonResponse(data)

from django.template.loader import get_template
from django.template import Context


class EnviarCorreo(object):
    titulo = ''
    template = ''
    def enviarCorreo(self, correos,titulo,template,parametrosHTML):
        self.titulo = titulo
        self.template = template
        self.parametrosHTML = parametrosHTML
        contenido_html = get_template(self.template).render(
            self.parametrosHTML
        )
        try:
            emailConfig = EmailMultiAlternatives(subject=self.titulo, 
                body=contenido_html, from_email='no-reply@gmail.com' , to=correos) #reply_to=[copiaPara,])
            emailConfig.content_subtype = "html"
            emailConfig.send(fail_silently=False)
        except Exception as e:
            print('Correo no enviado: ', str(e))
            error = {'error': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
            return error
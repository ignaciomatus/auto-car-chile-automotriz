from django.contrib import admin

# Register your models here.
from .models import Accident

class AccidentAdmin(admin.ModelAdmin):
    list_display = ["fecha","poliza","estado","grua","taller","liquidador","estado"]


admin.site.register(Accident, AccidentAdmin)
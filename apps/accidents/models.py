from apps.sucursal.models import TallerModel, ComunaModel, GruaModel
from django.db.models.constraints import UniqueConstraint
from django.db.models.fields.related import ForeignKey
from django.db.models.fields import DateField
from django.contrib.auth.models import User
from shortuuidfield import ShortUUIDField
from apps.clients.models import Poliza
from django.forms import model_to_dict
from django.db.models import Q
from datetime import datetime
from django.db import models


# scopes

class PendingAccidentsScope(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(Q(grua=None) | Q(liquidador=None) | Q(taller=None))

# Create your models here.

accident_statuses = [
    [0, "Nuevo"],
    [1, "Asignada"],
    [2, "Grua en camino"],
    [3, "En taller"],
    [4, "Cotizado"],
    [5, "Reparando"],
    [6, "Terminado"],
]

class Accident(models.Model):
    fecha = models.DateTimeField(default=datetime.now)
    # poliza_nroDoc = ShortUUIDField(editable=False, unique=True)
    detalles = models.TextField(null=False,default=None)
    poliza = models.ForeignKey(Poliza,related_name="poliza_involucrada",on_delete=models.CASCADE)
    estado = models.IntegerField(choices=accident_statuses,default=0)
    grua = models.ForeignKey(GruaModel, on_delete=models.CASCADE, blank=True, null=True)
    taller = models.ForeignKey(TallerModel, on_delete=models.PROTECT, blank=True, null=True)
    comuna = models.ForeignKey(ComunaModel, on_delete=models.PROTECT , blank=True, null=True)
    liquidador = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    direccion_incidente = models.CharField(max_length=50)
    activo = models.BooleanField(default=True)
    original_grua = None
    original_taller = None
    original_liquidador = None

    # objects = models.Manager()
    # pendings_objects = PendingAccidentsScope()
    
    def __init__(self, *args, **kwargs):
        super(Accident, self).__init__(*args, **kwargs)
        self.original_grua = self.grua
        self.original_taller = self.taller
        self.original_liquidador = self.liquidador

    def __str__(self) -> str:
        return str(self.id)
    
    def to_json(self):
        data = model_to_dict(self)
        data['estado'] = accident_statuses[self.estado][1]
        return data

    class Meta:
        ordering = ['fecha']
        db_table = 'Siniestros'
        default_permissions = ()
        permissions = (
            ("view_accidente",     "Ver Accidente/Siniestro"),
            ("add_accidente",      "Agregar Accidente/Siniestro"),
            ("change_accidente",   "Modificar Accidente/Siniestro"),
            ("delete_accidente",   "Eliminar Accidente/Siniestro"),
            ("asign_accidente",   "Asignar Accidente/Siniestro"),
            ("report_accidente",   "Ver reportes Accidente/Siniestro"),
        )

    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     print(self.grua)
    #     if self.pk:
    #         accidente = Accident.objects.filter(activo=True)
            

    #     super().save()
    #     print(self.grua)
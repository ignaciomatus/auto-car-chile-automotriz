from apps.sucursal.events import correo_asignacion_chofer
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Accident


@receiver(post_save, sender=Accident)
def valid_acupped(sender,instance, **kwargs):

    # si hay un cambio de gruas se realizan acciones
    if(instance.grua != instance.original_grua):
        # si la grua original no es nula se libera
        if instance.original_grua:
            instance.original_grua.liberate()
        # si se le asigna una grua esta se ocupa 
        if instance.grua:
            if instance.taller:
                correo_asignacion_chofer(instance)
            Accident.objects.filter(pk=instance.pk).update(estado=1)
            instance.grua.occupy()

        if instance.original_grua and instance.grua:
            # aplicando un pequenio truco si se usa el metodo save 
            # en un signal eso puede generar un ciclo infinito
            # se utiliza un query filter y update para pasar sobre el signal
            Accident.objects.filter(pk=instance.pk).update(estado=5)

    
    if(instance.liquidador != instance.original_liquidador):
        if instance.original_liquidador:
            instance.original_liquidador.perfilusuario.liberate()
        if instance.liquidador:
            instance.liquidador.perfilusuario.occupy()
    
    if(instance.taller != instance.original_taller):
        if instance.original_taller:
            instance.original_taller.liberate()
        if instance.taller:
            if instance.grua:
                correo_asignacion_chofer(instance)
            instance.taller.occupy()
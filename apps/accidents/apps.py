from django.apps import AppConfig

class AccidentsConfig(AppConfig):
    # According to the documentation, AppConfig.name is a full python path to the application.
    name = 'apps.accidents'

    def ready(self):
       import apps.accidents.signals
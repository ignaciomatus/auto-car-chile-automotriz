$(function(){

    var buttonCommon = {
        exportOptions: {
            columns: [ 0, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            format: {
                header: function ( data, columnIdx ) {
                    encontrado = data.indexOf("<select");
                    if (encontrado != -1) data = data.substring(0,encontrado);
                    return data;
                  }
            }
        }
    };


    tablaActual = $('.tablaActual').DataTable({
        responsive: true,
        autoWidth: false,
        destroy : true,
        aaSorting: [],
        "oLanguage": {
            "sSearch": "Buscar (RUT o nombre cliente)"
        },
        "columnDefs": [
            { "targets": [1], "visible": false }
        ],
        dom: 'Bfrtip',
        
        buttons: [
            $.extend( true, {}, buttonCommon, {
                extend: 'excelHtml5',
                text: 'Generar EXCEL',
                className : 'btn btn-success'
            }),
            $.extend( true, {}, buttonCommon, {
                extend: 'pdfHtml5',
                text: 'Generar PDF',
                className : 'btn btn-danger',
                 orientation: 'landscape',
                 title : 'Reporte de siniestros'
            })
        ],
        initComplete: function () {
                var footers = $('tfoot th');
                for (i = 0; i < footers.length; i++) {
                    this.api().order.listener(footers[i], i);
                }
            this.api().columns([0,3,6,7,8,9,10]).every( function () {
                    var column = this;
                    titulo = 'idX_' + this.header().textContent.replace(" ", "_");
                    tituloSimple =  this.header().textContent;
                    if (column.index() == 0) tituloSimple = "Mes";
                    var select = $('<select  class="form-control select2" id="' + titulo +  '"><option value="">-----</option></select>')
                        .appendTo( $(column.header()) )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            if (column.index() == 0){
                                
                                tablaActual.columns([0,1]).search(val).draw();
                            }else{
                                column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                            }                            
                        } );
                    
    
                     if (column.index() == 0){
                    meses = ['Enero', 'Febrero', 'Marzo', 'Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                    for (val of meses){
                        select.append( '<option value="'+val+'">'+val+'</option>' )
                    }
                    
                }else{
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                

                 $('#' + titulo).select2({
                    destroy : true,
                    placeholder: "Seleccionar  " +  tituloSimple,
                    closeOnSelect: true,
                    theme: 'classic',
                    width: "100%",
                    allowClear : true,
                });

            } );
        }
    });
    

})
    
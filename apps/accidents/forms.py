from apps.sucursal.models import GruaModel
from django.forms import fields, models
from .models import Accident, TallerModel
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from apps.sucursal.events import correo_asignacion_chofer


class AccidentForm(forms.ModelForm): 

    class Meta:
        model = Accident
        fields = ('poliza','detalles','fecha','direccion_incidente','comuna')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control','autocomplete':'off'})
        self.fields['poliza'].widget = forms.HiddenInput()


class AssignAccidentForm(forms.ModelForm):

    class Meta:
        model = Accident
        fields = ('liquidador',)#,'taller','grua',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control','autocomplete':'off'})


        if self.instance.pk:
            self.fields['liquidador'].queryset = User.objects.filter(Q(pk=0 if self.instance.liquidador == None else self.instance.liquidador.pk) | Q(groups__name__icontains='liquidador', perfilusuario__siniestros_disponible = True,
                perfilusuario__sucursal__comuna=self.instance.comuna))

class AssignWorkShopAccidentForm(forms.ModelForm):
    class Meta:
        model = Accident
        fields = ('taller',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control','autocomplete':'off'})
        if self.instance.pk:
            self.fields['taller'].queryset = TallerModel.objects.filter(Q(pk=0 if self.instance.taller == None else self.instance.taller.pk) | Q(comuna=self.instance.comuna, activo=True, reparaciones_disponible=True))
  
class AssignCraneAccidentForm(forms.ModelForm):
    class Meta:
        model = Accident
        fields = ('grua',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control','autocomplete':'off'})
        if self.instance.pk:
            self.fields['grua'].queryset = GruaModel.objects.filter( Q(pk=0 if self.instance.grua == None else self.instance.grua.pk) | Q(activo=True, comuna=self.instance.comuna,disponible=True ))  

class AssignWorkCraneAccidentForm(forms.ModelForm):
    class Meta:
        model = Accident
        fields = ('taller','grua',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control','autocomplete':'off'})
        if self.instance.pk:
            self.fields['taller'].queryset = TallerModel.objects.filter(Q(pk=0 if self.instance.taller == None else self.instance.taller.pk) | Q(comuna=self.instance.comuna, activo=True, reparaciones_disponible=True))
            self.fields['grua'].queryset = GruaModel.objects.filter( Q(pk=0 if self.instance.grua == None else self.instance.grua.pk) | Q(activo=True, comuna=self.instance.comuna,disponible=True ))  

from .forms import AccidentForm,AssignAccidentForm,AssignWorkShopAccidentForm,AssignCraneAccidentForm,AssignWorkCraneAccidentForm
from apps.accidents.models import Accident,accident_statuses
from django.http import HttpResponseRedirect,JsonResponse
from apps.reparacion.models import ReparacionModel
from django.http import HttpRequest,HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from apps.core.mixins import PermisosRequeridos
from django.urls import reverse_lazy, reverse
from django.shortcuts import render,redirect
from apps.sucursal.models import TallerModel
from django.views.generic import CreateView
from django.views.generic import ListView
from apps.clients.models import Poliza
from django.contrib import messages
from django.http import Http404
from django.db.models import Q


# Create your views here.


class NewAccident(CreateView):
    #pemission_required = ['accidents.add_accidente']
    form_class  = AccidentForm
    template_name = "new_accident.html"
    errors = ''

    def get_policy(self, **kwargs):
        try:
            queryset = Poliza.objects.filter(pk=self.kwargs['pk'])
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404("Poliza Record no found")
        return obj
    
    def form_valid(self, form):
        return super().form_valid(form)

    def form_invalid(self, form):
        # messages.error(self.request, self.error_message)
        self.errors = form.errors
        return super().form_invalid(form)
    
    def dispatch(self,*args,**kwargs):
        policy = self.get_policy(**kwargs)
        accident = Accident.objects.filter(poliza=policy).last()
        if accident:
            if not self.request.user.is_authenticated:
                return redirect('accidents:view_accident', pk=accident.pk)
            else:
                if not self.request.user.has_perms(['accidents.add_accidente']):
                    messages.error(self.request, 'No tiene permiso para ingresar a este módulo')
                    return HttpResponseRedirect(reverse_lazy('poliza'))
            if accident.estado != 6:
                messages.error(self.request, 'No es posible registrar otro siniestro hasta que el siniestro este completo')
                return redirect('accidents:view_accident', pk=accident.pk)
        else:
            if not self.request.user.is_authenticated:
                messages.error(self.request, 'No se ha encontrado un siniestro activo para la RUT asignada' )
                return HttpResponseRedirect(reverse_lazy('poliza'))
        return super().dispatch(*args,**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        policy = self.get_policy(**kwargs)
        context['policy'] = policy
        context['form'] = AccidentForm(initial={'poliza':policy.pk})
        context['tituloPagina'] = 'Registrar nuevo siniestro'
        context['errors'] = self.errors
        return context
    
    def get_success_url(self):
        return reverse('accidents:view_accident', kwargs={'pk' : self.object.pk})

class DetailAccident(DetailView):
    model = Accident
    template_name = 'detail_accident.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['status'] = accident_statuses[context['object'].estado][1]
        context['tituloPagina'] = 'Detalles del siniestro'
        context['tituloSeccion'] = 'Detalles del siniestro'
        context['cotizacion'] = ReparacionModel.objects.filter(siniestro=context['object']).first()
        return context

class ListPendingAccidentView(PermisosRequeridos,ListView):
    model = Accident
    template_name='list_pending_accident.html'
    permission_required = ['accidents.asign_accidente']
    queryset=Accident.objects.filter(Q(grua=None) | Q(liquidador=None) | Q(taller=None))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de Siniestros sin asignar'
        context['tituloSeccion'] = context['tituloPagina']
        context['estados'] = accident_statuses
        context['titulo'] = 'Siniestros'
        context['url'] = reverse_lazy('accidents:list_accidents')
        return context

class ListAccidentView(PermisosRequeridos,ListView):
    model = Accident
    template_name='list_accident.html'
    permission_required = ['accidents.view_accidente']
    #queryset=Accident.objects.exclude(Q(liquidador=None) | Q(taller=None))

    #def get_queryset(self):
    def get_queryset(self):
        q = self.model.objects.all().order_by('estado')
        grupos = self.request.user.groups.all()
        if grupos.filter(name__icontains='Taller').exists():
            q = q.filter(taller__in=TallerModel.objects.filter(encargado=self.request.user))
        return q

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de Siniestros para gestionar'
        context['tituloSeccion'] = context['tituloPagina']
        context['estados'] = accident_statuses

        context['titulo'] = 'Siniestros'
        context['url'] = reverse_lazy('accidents:list_accidents')
        return context

class ListAllAccidentView(PermisosRequeridos,ListView):
    model = Accident
    template_name='list_accident.html'
    permission_required = ['accidents.view_accidente']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de Siniestros'
        context['tituloSeccion'] = context['tituloPagina']
        context['estados'] = accident_statuses
        context['titulo'] = 'Siniestros'
        context['url'] = reverse_lazy('accidents:list')
        return context

class ReportesAccidentView(PermisosRequeridos,ListView):
    model = Accident
    template_name='report_accident.html'
    permission_required = ['accidents.report_accidente']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Tabla de reportes'
        context['tituloSeccion'] = context['tituloPagina']
        context['estados'] = accident_statuses
        context['titulo'] = 'Tabla de reportes'
        context['url'] = reverse_lazy('accidents:list')
        return context

class AssignAccident(PermisosRequeridos,UpdateView):
    permission_required = ['accidents.change_accidente']
    form_class  = AssignAccidentForm
    template_name = "edit_accident.html"
    model = Accident
    
    def get_success_url(self):
        return reverse('accidents:list_accidents')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tituloPagina"] = "Asignar Siniestro"
        context["tituloSeccion"] = "Asignar Siniestro"
        context["asignacion"] = True

        context['estados'] = accident_statuses
        return context

class AssignWorkshopCraneAccident(PermisosRequeridos,UpdateView):
    permission_required = ['accidents.change_accidente']
    form_class  = AssignWorkCraneAccidentForm
    template_name = "edit_accident.html"
    model = Accident
    
    def get_success_url(self):
        return reverse('accidents:list_accidents')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tituloPagina"] = "Asignar Taller"
        context["tituloSeccion"] = "Asignar Taller"
        context["asignacion"] = True

        context['estados'] = accident_statuses
        return context
        
class AssignWorkShopAccident(PermisosRequeridos,UpdateView):
    permission_required = ['accidents.change_accidente']
    form_class  = AssignWorkShopAccidentForm
    template_name = "edit_accident.html"
    model = Accident
    
    def get_success_url(self):
        return reverse('accidents:list_accidents')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tituloPagina"] = "Asignar Taller"
        context["tituloSeccion"] = "Asignar Taller"
        context["asignacion"] = True

        context['estados'] = accident_statuses
        return context

class AssignCraneAccident(PermisosRequeridos,UpdateView):
    permission_required = ['accidents.change_accidente']
    form_class  = AssignCraneAccidentForm
    template_name = "edit_accident.html"
    model = Accident
    
    def get_success_url(self):
        return reverse('accidents:list_accidents')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tituloPagina"] = "Asignar Grua"
        context["tituloSeccion"] = "Asignar Grua"
        context["asignacion"] = True

        context['estados'] = accident_statuses
        return context
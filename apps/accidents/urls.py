from .views import (AssignCraneAccident, AssignWorkShopAccident, ListAccidentView, ListPendingAccidentView, NewAccident,DetailAccident,AssignAccident,ListAllAccidentView,AssignWorkshopCraneAccident, ReportesAccidentView)
from django.urls import path

app_name = 'accidents'

urlpatterns = [
    path('new-accident/<int:pk>',NewAccident.as_view(),name='add_accident'),
    path('accident/<int:pk>',DetailAccident.as_view(),name='view_accident'),
    path('accident/list_pending', ListPendingAccidentView.as_view(), name="pending_list"),
    path('accident/list', ListAccidentView.as_view(), name="list_accidents"),
    path('accident/list/report', ReportesAccidentView.as_view(), name="report_accidents"),
    path('accident/assing/<int:pk>', AssignAccident.as_view(), name="assing"),
    path('accident/assing/workshop/<int:pk>', AssignWorkshopCraneAccident.as_view(), name="assing_workshop"),
    # path('accident/assing/crane/<int:pk>', AssignCraneAccident.as_view(), name="assing_crane"),
    path('accident/list/all', ListAllAccidentView.as_view(), name="list"),
]


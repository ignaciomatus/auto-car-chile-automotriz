from django.urls import path, include
from .viewsReparacion import ListReparacionView, AddReparacionView, ChangeReparacionView, DeleteReparacionView, aproved_fix, finish_fix
from .views import edit, get_detail, new_reparacion,storage,update,pdf
# from .viewsTipoReparacion import ListTipoReparacionView, AddTipoReparacionView,ChangeTipoReparacionView, DeleteTipoReparacionView
urlpatterns = [
    path('list', ListReparacionView.as_view(), name="listReparacion"),
    path('register/<int:pk>', new_reparacion, name="addReparacion"),
    path('storage', storage, name="storageReparacion"),
    path('change/<int:pk>',ChangeReparacionView.as_view(),name = 'changeReparacion'),
    path('aproved/<int:pk>/',aproved_fix,name = 'aproved_fix'),
    path('finish/<int:pk>/',finish_fix,name = 'finish_fix'),
    path('delete/<int:pk>/',DeleteReparacionView.as_view(),name = 'deleteReparacion'),
    path('edit/<int:pk>',edit,name = 'editReparacion'),
    path('pdf/<int:pk>',pdf,name = 'pdfReparacion'),
    path('update',update,name = 'updateReparacion'),
    path('get/detail/<int:pk>',get_detail,name = 'detailReparacion'),

    # path('tipo/list', ListTipoReparacionView.as_view(), name="listTipoReparacion"),
    # path('tipo/register', AddTipoReparacionView.as_view(), name="addTipoReparacion"),
    # path('tipo/change/<int:pk>/',ChangeTipoReparacionView.as_view(),name = 'changeTipoReparacion'),
    # path('tipo/delete/<int:pk>/',DeleteTipoReparacionView.as_view(),name = 'deleteTipoReparacion'),
    

]

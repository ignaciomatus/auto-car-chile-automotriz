from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.views.generic.base import RedirectView
from django_xhtml2pdf.utils import pdf_decorator
from django.http import JsonResponse, response
from django.shortcuts import render, redirect
from apps.accidents.models import Accident
from apps.core.mixins import EnviarCorreo
from .helpers import edit_fix, store_fix
from django.urls import reverse_lazy
from .models import ReparacionModel
from django.db import reset_queries
from .forms import ReparacionForm
import json

# class GruaNuevoServicioView(EnviarCorreo,RedirectView):
#      is_permanent = True

#      def get_redirect_url(self,*args, **kwargs):
         
#          return reverse_lazy('listReparacion')
@login_required
@require_http_methods(["GET"])
def new_reparacion(request,pk):
    context = {'form':ReparacionForm()}
    context['object'] = Accident.objects.get(pk=pk)
    return render(request,'new_fix.html',context)

@login_required
@require_http_methods(["POST"])
def storage(request):
    response = store_fix(request.POST)
    return JsonResponse(response,safe=False,status=response['code'])

@login_required
@require_http_methods(["GET"])
def edit(request,pk):
    fix = ReparacionModel.objects.get(pk=pk)
    context = {'object':Accident.objects.get(pk=fix.siniestro.pk),'pk':pk}
    return render(request,'edit_fix.html',context)

@login_required
@require_http_methods(["GET"])
def get_detail(request,pk):
    response = (ReparacionModel.objects.get(pk=pk)).to_json()
    return JsonResponse(response,safe=False)

@login_required
@require_http_methods(["POST"])
def update(request):
    response = edit_fix(json.loads(request.body))
    return JsonResponse(response,safe=False,status=response['code'])

@login_required
@require_http_methods(["GET"])
@pdf_decorator(pdfname='reparacion.pdf')
def pdf(request,pk):
    context = {'object':ReparacionModel.objects.get(pk=pk)}
    return render(request,'fix_pdf.html',context)
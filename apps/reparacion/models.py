from django.db import models
from django.db.models.fields import BooleanField
from django.db.models.fields.related import ManyToManyField
from django.forms.models import model_to_dict
from apps.accidents.models import Accident
# Create your models here.
class TipoReparacionModel(models.Model):
    nombre = models.CharField(max_length = 150)
    descripcion = models.CharField(max_length=254)
    monto = models.PositiveIntegerField(default=0)
    
    def __str__(self):
        return self.descripcion
    
    def to_json(self):
        data = model_to_dict(self)
        return data

    class Meta:
        db_table = 'tipo_reparacion'
        default_permissions = ()
        permissions = (
            ("view_tipoReparacion",     "Ver tipo de reparación"),
            ("add_tipoReparacion",      "Registrar tipo de reparación"),
            ("change_tipoReparacion",   "Actualizar tipo de reparación"),
            ("delete_tipoReparacion",   "Eliminar tipo de reparación"),
        )

class ReparacionModel(models.Model):
    montoCobertura = models.PositiveIntegerField(default=0)
    montoTotal = models.PositiveIntegerField(default=0)
    tiempoRepDias = models.PositiveIntegerField(default=0)
    siniestro = models.ForeignKey(Accident, on_delete=models.CASCADE)
    detalles = ManyToManyField(TipoReparacionModel,related_name="detalles_de_reparacion")
    autorizado = models.BooleanField(default=False)
    observaciones = models.TextField(default=None,null=True)
    # vehiculo = models.ForeignKey(Vehiculo, on_delete=models.PROTECT)
    # cliente = models.ForeignKey(Cliente, on_delete=models.PROTECT)
    # liquidador = models.ForeignKey(User, on_delete=models.PROTECT)
    #Quitar vehiculo, cliente, liquidador, y asignar siniestro, dejando la visualizacion de cliente y vehiculo
    def __str__(self):
        return "{} {}".format(self.siniestro.poliza.cliente, self.siniestro.poliza.vehiculo)
    
    def to_json(self):
        data = model_to_dict(self,exclude=('detalles','autorizado','siniestro',))
        data['details'] = []
        for type_fix in self.detalles.all():
            data['details'].append(type_fix.to_json())
        return data

    class Meta:
        db_table = 'reparacion'
        default_permissions = ()
        permissions = (
            ("view_reparacion",     "Ver reparacion"),
            ("add_reparacion",      "Registrar reparacion"),
            ("change_reparacion",   "Actualizar reparacion"),
            ("delete_reparacion",   "Eliminar reparacion"),
            ("accept_reparacion",   "Aceptar reparación/Presupuesto"),
        )

# class DetalleModelo(models.Model):
    # tipo_reparacion = models.ForeignKey(TipoReparacionModel, on_delete=models.PROTECT)
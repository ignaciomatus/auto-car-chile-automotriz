from .models import ReparacionModel, TipoReparacionModel
from apps.accidents.models import Accident
from apps.core.mixins import EnviarCorreo
from .forms import ReparacionForm

def store_fix(request_data,instance = None):
    response = {'code':200}
    form = ReparacionForm(request_data)
    if instance:
        form = ReparacionForm(request_data,instance=instance)
    if form.is_valid():
        fix = form.save(commit=False)
        fix.save()
        store_details(request_data,fix)
        response['id'] = fix.pk
        send_fix_email(fix,'Se realizo presupuesto')
    else:
        response["status"] = "error"
        response["code"] = 400
        response["details"] = ""
        for field, errors in form.errors.items():
            response["details"] += str(field)+" : "+str(errors.as_text()+"    ")
    return response

def store_details(data,fix):
    count = len(data.getlist("nombre"))
    total = 0
    for x in range(count):
        fix.detalles.create(
            nombre = data.getlist("nombre")[x],
            descripcion =  data.getlist("descripcion")[x],
            monto =  data.getlist("monto")[x],
        )
        total = total + int(data.getlist("monto")[x])
    if(fix.montoTotal != total):
        fix.montoTotal = total
    Accident.objects.filter(pk= fix.siniestro.pk).update(estado=4)
    fix.save()

def edit_fix(data,instance = None):
    response = {'code':200}
    ReparacionModel.objects.update_or_create(pk=data['id'],defaults=data)
    fix = ReparacionModel.objects.get(pk=data['id'])
    TipoReparacionModel.objects.filter(detalles_de_reparacion=data['id']).delete()
    for detail in data['details']:
        fix.detalles.update_or_create(pk=detail['id'],defaults=detail)
    send_fix_email(fix,'Ajuste Presupuesto')
    return response

def send_fix_email(fix,subject):
    EnviarCorreo().enviarCorreo(
        ["l.ignaciomatus@gmail.com","williagut@gmail.com","jos.aravenas@alumnos.duoc.cl",fix.siniestro.poliza.cliente.email],
        subject,
        'correos/presupuesto.html',
        {
            'accidente':fix.siniestro,
            'cotizacion':fix,
            'dominio':'127.0.0.1:8000',
        }
    )
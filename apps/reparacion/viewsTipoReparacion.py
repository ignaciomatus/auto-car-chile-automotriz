from django.shortcuts import render
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from .models import TipoReparacionModel
from .forms import TipoReparacionForm
from django.urls import reverse_lazy
from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo

class ListTipoReparacionView(PermisosRequeridos,ListView):
    model = TipoReparacionModel
    template_name='listTipoReparacion.html'
    permission_required = ['reparacion.view_tipoReparacion']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de tipos de reparación registradas'
        context['titulo'] = 'Tipo de reparación'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listTipoReparacion')
        return context

class AddTipoReparacionView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = TipoReparacionModel
    form_class = TipoReparacionForm
    permission_required =   ['reparacion.add_tipoReparacion']
    success_url = reverse_lazy('listTiporeparacion')
    mensajeExito = 'Tipo de reparación registrada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar tipo de reparación'
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['columnaForm'] = True
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listTipoReparacion')
        return context

class ChangeTipoReparacionView(PermisosRequeridos,GuardarModelo, UpdateView):
    template_name = 'base/baseForm.html'
    model = TipoReparacionModel
    form_class = TipoReparacionForm
    permission_required =   ['reparacion.change_tipoReparacion']
    success_url = reverse_lazy('listTiporeparacion')
    mensajeExito = 'Datos del tipo de reparación actualizados exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar tipo de reparación ' + str(self.object.nombre)
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  "Actualizar tipo de reparación"
        context["action"] = "edit"
        context['columnaForm'] = True
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listTipoReparacion')
        return context

class DeleteTipoReparacionView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = TipoReparacionModel
    permission_required =   ['reparacion.delete_tipoReparacion']
    mensajeExito = "Tipo de reparación eliminada exitosamente."
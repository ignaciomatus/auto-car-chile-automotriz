from apps.core.mixins import StaffPermiso, PermisosRequeridos, GuardarModelo, BorrarModelo
from apps.core.decorators import group_required,superuser_required,permission_required
from django.views.generic import FormView, ListView, UpdateView, DeleteView
from django.views.decorators.http import require_http_methods
from django.views.generic.detail import DetailView
from django.http import JsonResponse, response
from apps.accidents.models import Accident
from django.urls import reverse_lazy
from .models import ReparacionModel
from django.shortcuts import render
from django.db.models import query
from .forms import ReparacionForm

class ListReparacionView(PermisosRequeridos, ListView):
    model = ReparacionModel
    template_name='listReparacion.html'
    permission_required = ['reparacion.view_reparacion']
    queryset = ReparacionModel.objects.filter(autorizado=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tituloPagina'] = 'Lista de reparaciones sin asignar'
        context['titulo'] = 'Reparación'
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listReparacion')
        return context

class AddReparacionView(PermisosRequeridos,GuardarModelo, FormView):
    template_name = 'base/baseForm.html'
    model = ReparacionModel
    form_class = ReparacionForm
    permission_required =   ['reparacion.add_reparacion']
    success_url = reverse_lazy('listReparacion')
    mensajeExito = 'Reparación registrada exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Registrar reparación'
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  titulo
        context["action"] = "add"
        context['tituloSeccion'] = context['tituloPagina']
        context['columnaForm'] = True
        context['url'] = reverse_lazy('listReparacion')
        return context

class ChangeReparacionView(PermisosRequeridos,GuardarModelo, DetailView):
    template_name = 'aproved.html'
    model = ReparacionModel
    # form_class = ReparacionForm
    permission_required =   ['reparacion.change_reparacion']
    success_url = reverse_lazy('listReparacion')
    mensajeExito = 'Datos de la reparación actualizados exitosamente'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        titulo = 'Actualizar reparación del cliente {} con el vehiculo {}'.format(self.object.siniestro.poliza.cliente, self.object.siniestro.poliza.vehiculo)
        context['tituloPagina'] = titulo
        context['tituloForm'] = titulo
        context['botonEnviar'] =  "Actualizar reparación"
        context["action"] = "edit"
        context['columnaForm'] = True
        context['tituloSeccion'] = context['tituloPagina']
        context['url'] = reverse_lazy('listReparacion')
        return context

@require_http_methods(["POST"])
@permission_required('reparacion.change_reparacion',)
def aproved_fix(request,pk):
    response = {'status':'200'}
    fix = ReparacionModel.objects.get(pk=pk)
    fix.autorizado=True
    fix.save()
    Accident.objects.filter(poliza_id=fix.siniestro.poliza.pk).update(estado=5)
    return JsonResponse(response,safe=False,status=response['status'])

@require_http_methods(["POST"])
@permission_required('reparacion.change_reparacion',)
def finish_fix(request,pk):
    response = {'status':'200'}
    Accident.objects.filter(pk=pk).update(estado=6)
    return JsonResponse(response,safe=False,status=response['status'])

class DeleteReparacionView(PermisosRequeridos,BorrarModelo, DeleteView):
    model = ReparacionModel
    permission_required =   ['reparacion.delete_reparacion']
    mensajeExito = "Reparación eliminada exitosamente."
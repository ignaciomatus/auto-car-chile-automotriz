from apps.core.forms import IncializarFormulario
from django import forms
from .models import TipoReparacionModel, ReparacionModel

class TipoReparacionForm(IncializarFormulario):
    nombre = forms.CharField(label="Nombre del tipo de reparación*",widget=forms.TextInput(attrs={'placeholder': 'Nombre del tipo de reparación'}))
    descripcion = forms.CharField(label="Descripción del tipo de reparación*",widget=forms.Textarea(attrs={'placeholder': 'Breve descripción del tipo de reparación','rows':3,
                                            'style':'resize:none;'}))

    class Meta:
        model = TipoReparacionModel
        fields = ['nombre', 'descripcion']

class ReparacionForm(IncializarFormulario):
    class Meta:
        model = ReparacionModel
        exclude = ('detalles','autorizado')
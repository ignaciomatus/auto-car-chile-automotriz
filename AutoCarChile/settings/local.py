from AutoCarChile.settings.base import *

ALLOWED_HOSTS = ['*']

#Example

#THIRD_PARTY_APPS += [
#    'debug_toolbar',
#    
#]

#INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "../static"),
]

MEDIA_ROOT = os.path.join(BASE_DIR, "../media")

# Media files avatars that users uploads to perfil
MEDIA_URL = '/media/'
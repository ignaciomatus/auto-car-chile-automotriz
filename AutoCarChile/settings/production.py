from AutoCarChile.settings.base import *

#SET DEBUF FALSE TO PRODUCTION
DEBUG=False

THIRD_PARTY_APPS+=[
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

STATIC_ROOT='/var/www/AutoCarChile/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR,"../static"),
]

MEDIA_ROOT = "/var/www/AutoCarChile/media/"